import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

interface MensajesData {
  titulo: string;
  texto: string;
}

@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.component.html',
  styleUrls: ['./informacion.component.css']
})
export class InformacionComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: MensajesData) { }

  ngOnInit() {
  }

}
