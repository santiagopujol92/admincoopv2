import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

interface MensajesData {
  titulo: string;
  texto: string;
}

@Component({
  selector: 'app-confirmacion',
  templateUrl: './confirmacion.component.html',
  styleUrls: ['./confirmacion.component.css']
})
export class ConfirmacionComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: MensajesData) { }

  ngOnInit() {
  }

}
