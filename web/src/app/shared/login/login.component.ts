import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent {

  formulario: FormGroup;
  matSpinner = false;

  // Constructor
  constructor(private formBuilder: FormBuilder,
    private _loginService: LoginService,
    private router: Router,
    public snackBar: MatSnackBar,
  ) {

    // HARCODEADO LOGIN - DEJARLO VACIO LUEGO
    this.formulario = this.formBuilder.group({
      username: ['admin2', Validators.required],
      password: ['asd123', Validators.required],
    });
  }

  loginUsuario(dataForm) {
    if (dataForm.status === 'VALID') {
      this.matSpinner = true;
      this._loginService.loginPost(dataForm.value).subscribe(user => {
        // Logeado
        if (user.estado === 1) {
          this.snackBar.open('Usuario Logeado con Êxito', '', { duration: 2500, panelClass: ['success-snackbar'], verticalPosition: 'top', horizontalPosition: 'right' });
          // Guardar data en Auth:
          this._loginService.saveUserLogged(user.data);
          this.router.navigateByUrl('\dashboard');
        // Usuario deshabilitado
        } else if (user.estado === -1) {
          this.snackBar.open('Atencion !! Usuario Deshabilitado', '', { duration: 5000, panelClass: ['danger-snackbar'], verticalPosition: 'top', horizontalPosition: 'center' });
        // Usuario o Contraseña incorrecta
        } else {
          this.snackBar.open('Usuario y/o Contraseña Incorrecta', '', { duration: 5000, panelClass: ['warning-snackbar'], verticalPosition: 'top', horizontalPosition: 'center' });
        }
        this.matSpinner = false;
      }, err => {
        this.snackBar.open('Ocurrió un error al intentar conectarse al servidor', '', { duration: 5000, panelClass: ['danger-snackbar'], verticalPosition: 'top', horizontalPosition: 'center' });
        this.matSpinner = false;
      });
    }
  }
}
