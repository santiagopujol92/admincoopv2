import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { Usuario } from '../../models/Usuario';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private loginService: LoginService,
    private usuario: Usuario
  ) {
    this.usuario.name = localStorage.getItem('name');
    this.usuario.lastname = localStorage.getItem('lastname');
    this.usuario.type_description = localStorage.getItem('type_description');
    this.usuario.type = Number(localStorage.getItem('type'));
  }

  ngOnInit() {

  }

  cerrarSesion() {
    this.loginService.logOut();
  }

}
