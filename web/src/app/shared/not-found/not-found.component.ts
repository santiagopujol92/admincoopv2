import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor(
      private _loginService: LoginService,
      private snackBar: MatSnackBar,
      private router: Router
    ) { }

  ngOnInit() {
    if (this._loginService.getUserLogged() === null) {
      this.router.navigateByUrl('\login');
      this.snackBar.open('La sesión está desconectada, debe autentificarse', '', { duration: 2500, panelClass: ['danger-snackbar'], verticalPosition: 'top', horizontalPosition: 'center' });
    }
  }

}
