import { NgModule } from '@angular/core';

import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Angular Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule, ErrorStateMatcher, ShowOnDirtyErrorStateMatcher, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule, MatIconModule, MatDialogModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

/* import { MatFileUploadModule } from 'angular-material-fileupload'; */
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
// Componentes
import { NavbarComponent } from './navbar/navbar.component';
import { PAGES_ROUTES } from './shared.routes';
import { MatTabsModule } from '@angular/material/tabs';
import { ConfirmacionComponent } from './mensajes/confirmacion/confirmacion.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTooltipModule } from '@angular/material/tooltip';
import { InformacionComponent } from './mensajes/informacion/informacion.component';


@NgModule({
    declarations: [
        NotFoundComponent,
        LoginComponent,
        NavbarComponent,
        ConfirmacionComponent,
        InformacionComponent
    ],
    imports: [
        PAGES_ROUTES,
        BrowserAnimationsModule,
        MatCheckboxModule,
        MatToolbarModule,
        MatMenuModule,
        MatButtonModule,
        MatSortModule,
        MatIconModule,
        MatTableModule,
        MatFormFieldModule,
        HttpClientModule,
        FormsModule,
        MatInputModule,
        MatCardModule,
        MatDialogModule,
        MatDividerModule,
        MatListModule,
        MatSelectModule,
        /*         MatFileUploadModule, */
        MatTabsModule,
        MatProgressBarModule,
        MatSnackBarModule,
        MatExpansionModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTooltipModule
    ],
    exports: [
        NotFoundComponent,
        FormsModule,
        LoginComponent,
        MatCheckboxModule,
        NavbarComponent,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        MatSortModule,
        MatTableModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatInputModule,
        MatCardModule,
        MatDialogModule,
        MatDividerModule,
        MatSelectModule,
        MatListModule,
        /*       MatFileUploadModule, */
        MatTabsModule,
        MatProgressBarModule,
        MatSnackBarModule,
        MatMenuModule,
        MatExpansionModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
        ConfirmacionComponent,
        InformacionComponent,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTooltipModule
    ],
    entryComponents: [
        ConfirmacionComponent,
        InformacionComponent
    ],
    providers: [
        { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher },
    ]
})

export class SharedModule { }
