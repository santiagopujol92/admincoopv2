import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../pages/dashboard/dashboard.component';
import { PagesComponent } from '../pages/pages.component';
import { ProductosComponent } from '../pages/abm/productos/productos.component';
import { HojaDeRutaComponent } from 'src/app/pages/hoja-de-ruta/hoja-de-ruta.component';
import { NuevaHojaRutaComponent } from 'src/app/pages/hoja-de-ruta/nueva-hoja-ruta/nueva-hoja-ruta.component';
import { ReasignarHojaDeRutaComponent } from 'src/app/pages/hoja-de-ruta/reasignar-hoja-de-ruta/reasignar-hoja-de-ruta.component';

const pagesRoutes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'productos', component: ProductosComponent },
            { path: 'hoja-de-ruta', component: HojaDeRutaComponent },
            { path: 'nueva-hoja-ruta', component: NuevaHojaRutaComponent },
            { path: 'reasignar-hoja-de-ruta', component: ReasignarHojaDeRutaComponent },
            { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
        ]
    }
];


export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
