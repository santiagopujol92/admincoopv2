import { Pipe, PipeTransform } from '@angular/core';
import { ValueTransformer } from '@angular/compiler/src/util';

@Pipe({
  name: 'cuil'
})
export class CuilPipe implements PipeTransform {

  transform(val: string): any {

    const primeraParte = val.substring(0, 2);
    const segundaParte = val.substring(2, 10);
    const TerceraParte = val.substring(10, 11);
    const concatenar = primeraParte.concat('-' + segundaParte.concat('-' + TerceraParte));
    return concatenar;
  }
}

@Pipe({
  name: 'revomerGuiones-cuil'
})
export class RemoverGuionesPipe implements PipeTransform {

  transform(val: string): any {

    const primeraParte = val.substring(0, 2);
    const segundaParte = val.substring(2, 10);
    const TerceraParte = val.substring(10, 11);
    const concatenar = primeraParte.concat('-' + segundaParte.concat('-' + TerceraParte));
    return concatenar;
  }
}




