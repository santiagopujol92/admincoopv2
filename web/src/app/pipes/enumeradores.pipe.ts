import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';


@Pipe({
    name: 'tipo_negocio'
})
export class TipoNegocioPipe implements PipeTransform {
    transform(value: any, args?: any): string {
        switch (String(value)) {
            case '1':
                return 'NORMAL';
            case '2':
                return 'SUPERMERCADO';
            default:
                return '-';
        }

    }
}

@Pipe({
    name: 'estado_informante'
})
export class EstadoInformantePipe implements PipeTransform {
    transform(value: any): string {
        switch (String(value)) {
            case '1':
                return 'ACTIVO';
            case '2':
                return 'INACTIVO';
            case '3':
                return 'BAJA';
            default:
                return '-';
        }

    }
}

@Pipe({
    name: 'medio_captacion'
})
export class MedioCaptacionPipe implements PipeTransform {
    transform(value: any): string {
        switch (String(value)) {
            case '1':
                return 'CAMPO';
            case '2':
                return 'ESCRITORIO: MAIL';
            case '3':
                return 'ESCRITORIO: TELEFONO';
            case '4':
                return 'ESCRITORIO: WEB';
            default:
                return '-';
        }

    }
}

@Pipe({
    name: 'base_ref'
})
export class BaseRefPipe implements PipeTransform {
    transform(value: any): string {
        switch (String(value)) {
            case '1':
                return 'DE CAMPO';
            case '2':
                return 'INICIAL';
            case '3':
                return 'CIFCO';
            default:
                return '-';
        }

    }
}

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) { }
    transform(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}
