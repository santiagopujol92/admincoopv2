import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { APP_ROUTES } from './app.routes';
import { PagesModule } from './pages/pages.module';
import { MatDialogModule } from '@angular/material';
import { Usuario } from './models/Usuario';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    PagesModule,
    MatDialogModule
  ],
  entryComponents: [
  ],
  providers: [ Usuario ],
  bootstrap: [AppComponent]
})
export class AppModule { }
