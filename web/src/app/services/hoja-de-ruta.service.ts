import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConectionString } from '../models/ConectionString';

@Injectable({
  providedIn: 'root'
})
export class HojaDeRutaService {
  url: String = ConectionString.url;
  constructor(private http: HttpClient) { }

  obtenerListadoHojaRuta(): Observable<any> {
    return this.http.get<any>(this.url + 'api/HojaDeRuta');
  }

  asignarHojaDeRutaAEncuestador(listIdPlanificacion): Observable<any> {
    return this.http.get<any>(
      this.url + 'api/HojaDeRuta/AsignarEncuestadores/' + listIdPlanificacion
    );
  }

  insertarHojaRuta(hoja_ruta): Observable<any> {
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(hoja_ruta);
    return this.http.post<any>(this.url + 'api/HojaDeRuta',
      body, { headers: headerOptions });
  }

  obtenerHojaRutaByIdPlanificacion(idPlanificacion): Observable<any> {
    return this.http.get<any>(this.url + 'api/HojaDeRuta/GetHojaRutaByIdPlanificacion/' + idPlanificacion);
  }

  generacionHojaDeRuta(idUsuario): Observable<any> {
    return this.http.get<any>(this.url + 'api/HojaDeRuta/GenerarHojaDeRuta/' + idUsuario);
  }

  cargarMesAño(): Observable<any> {
    return this.http.get<any>(this.url + 'api/HojaDeRuta/ObtenerMesAño');
  }

  updateHojaRuta(hoja_ruta, id_planificacion): Observable<any> {
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(hoja_ruta);
    return this.http.put<any>(this.url + 'api/HojaDeRuta/' + id_planificacion,
      body, { headers: headerOptions });
  }

  obtenerListadoHRByMesesAnios(fecha): Observable<any> {
      return this.http.get<any>(this.url + 'api/HojaDeRuta/GetListadoByMesAnioHR/' + fecha);
  }

  reasignarEncuestador(hoja_ruta): Observable<any> {
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(hoja_ruta);
    return this.http.post<any>(this.url + 'api/HojaDeRuta/Reasignar_Encuestador',
      body, { headers: headerOptions });
  }

  reasignarInformantesHojaDeRuta(data_a_reasignar): Observable<any> {
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(data_a_reasignar);
    return this.http.post<any>(this.url + 'api/HojaDeRuta/ReasignarInformantesHojaDeRuta',
      body, { headers: headerOptions });
  }

  obtenerListaHojasDeRutaGeneradas(): Observable<any> {
    return this.http.get<any>(this.url + 'api/HojaDeRuta/GetHojaRutaGeneradas');
  }

  reasignarInformantesNuevaHojaDeRuta(data_a_reasignar): Observable<any> {
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(data_a_reasignar);
    return this.http.post<any>(this.url + 'api/HojaDeRuta/ReasignarInformantesNuevaHojaDeRuta',
      body, { headers: headerOptions });
  }

  bajaHojaRutaEInformantes(idPlanificacion, idEstadoBajaInformante): Observable<any> {
    return this.http.get<any>(this.url + 'api/HojaDeRuta/BajaHojaDeRutaEInformantes/' + idPlanificacion + '/' + idEstadoBajaInformante);
  }
}
