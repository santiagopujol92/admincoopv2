import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConectionString } from '../models/ConectionString';

@Injectable({
    providedIn: 'root'
})
export class UsuarioService {
    url: String = ConectionString.url;
    constructor(private http: HttpClient) {
    }

    obtenerListaRoles(): Observable<any> {
        return this.http.get<any>(this.url + 'api/Usuario/GetRoles');
    }

    obtenerUsuarioCidiByCuil(cuil): Observable<any> {
        return this.http.get<any>(this.url + 'api/Usuario/GetUser_ByCuil/' + cuil);
    }
    obtenerListaUsuarios(): Observable<any> {
        return this.http.get<any>(this.url + 'api/Usuario');
    }

    insertarUsuario(usuario): Observable<any> {
        const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
        const body = JSON.stringify(usuario);
        return this.http.post<any>(this.url + 'api/Usuario/' + usuario, body, { headers: headerOptions });
    }
    eliminarUsuario(cuil) {
        return this.http.delete<any>(this.url + 'api/Usuario/' + cuil);
    }

    obtenerListaCoordinadores(): Observable<any> {
        return this.http.get(this.url + 'api/Usuario/GetCoordinadores');
    }

    modificarUsuario(usuario): Observable<any> {
        const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
        const body = JSON.stringify(usuario);
        return this.http.put(this.url + 'api/Usuario/' + 22, body, { headers: headerOptions });
    }

    obtenerListaEncuestadores(): Observable<any> {
        return this.http.get(this.url + 'api/Encuestador');
    }

    obtenerListaSupervisores(idUsuario): Observable<any> {
        return this.http.get(this.url + 'api/Usuario/ObtenerListaSupervisores/' + idUsuario);
    }
}
