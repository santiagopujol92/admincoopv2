import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConectionString } from '../models/ConectionString';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  url: String = ConectionString.url;

  constructor(private http: HttpClient) { }

  obtenerListaProductos(): Observable<any> {
    return this.http.get<any>(this.url + 'api/Producto');
  }

  obtenerListaProductosXRubros(idsRubro: string): Observable<any> {
    return this.http.get<any>(this.url + 'api/Producto/GetProductoXRubros_GetAll/' + idsRubro);
  }

  eliminarProducto(idProducto): Observable<any> {
    return this.http.delete<any>(this.url + 'api/Producto/' + idProducto);
  }

  insertarProducto(producto): Observable<any> {
    console.log('entre');

    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(producto);
    return this.http.post<any>(this.url + 'api/Producto',
      body, { headers: headerOptions });
  }

  editarProducto(producto): Observable<any> {
    console.log(producto);

    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(producto);
    return this.http.put<any>(this.url + 'api/Producto/' + producto.Precio_Inicial,
      body, { headers: headerOptions });
  }

  asociarAtributosXProductos(atributoXProducto): Observable<any> {
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(atributoXProducto);
    return this.http.post<any>(this.url + 'api/Producto/ProductoXAtributos',
      body, { headers: headerOptions });
  }

  asociarRubrosXProductos(rubrosXProducto): Observable<any> {
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(rubrosXProducto);
    return this.http.post<any>(this.url + 'api/Producto/ProductoXRubros',
      body, { headers: headerOptions });
  }

  asociarIndicesXProductos(indicesXProducto): Observable<any> {
    const headerOptions = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(indicesXProducto);
    return this.http.post<any>(this.url + 'api/Producto/ProductoXIndices',
      body, { headers: headerOptions });
  }

  cargarAtributosXProductos(idProducto): Observable<any> {
    return this.http.get<any>(this.url + 'api/Producto/GetProductoXAtributos/' + idProducto);
  }

  cargarIndicesXProductos(idProducto): Observable<any> {
    return this.http.get<any>(this.url + 'api/Producto/GetProductoXIndices/' + idProducto);
  }

  cargarRubrosXProductos(idProducto): Observable<any> {
    return this.http.get<any>(this.url + 'api/Producto/GetProductoXRubros/' + idProducto);
  }

  obtenerProductosByInformanteParaAsignacion(idInformante) {
    return this.http.get<any>(this.url + 'api/Producto/GetProductosByIdInformanteParaAsignacion/' + idInformante);
  }
}
