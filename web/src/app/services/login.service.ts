import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConectionString } from '../models/ConectionString';
import { Router } from '@angular/router';
import { Usuario } from '../models/Usuario';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url: String = ConectionString.url;
  usuario: Usuario;

  constructor(private http: HttpClient,
    private router: Router,
    private snackBar: MatSnackBar
    ) {
      this.usuario = null;
    }

  loginPost(userData): Observable<any> {
    const headerOptions = new HttpHeaders({'Content-Type': 'application/json' });
    const $request = JSON.stringify(userData);
    return this.http.post<Usuario>(this.url + 'login', $request, { headers: headerOptions });
  }

  getUsers(): Observable<any> {
    return this.http.get<Usuario>(this.url + 'usuarios_listar');
  }

  getUserLogged() {
    if (localStorage.getItem('statusLogged') !== '0') {
      return localStorage;
    }
    return null;
  }

  saveUserLogged(dataUser) {
    this.usuario = dataUser[0];
    localStorage.setItem('id', dataUser[0].id);
    localStorage.setItem('username', dataUser[0].username);
    localStorage.setItem('email', dataUser[0].email);
    localStorage.setItem('name', dataUser[0].name);
    localStorage.setItem('lastname', dataUser[0].lastname);
    localStorage.setItem('type_description', dataUser[0].type_description);
    localStorage.setItem('type', dataUser[0].type);
    localStorage.setItem('status', dataUser[0].status);
    localStorage.setItem('statusLogged', '1');
  }

  logOut() {
    localStorage.clear();
    localStorage.setItem('statusLogged', '0');
    this.usuario = null;
    this.snackBar.open('Usted ha cerrado sesión con Èxito', '', { duration: 2500, panelClass: ['success-snackbar'], verticalPosition: 'top', horizontalPosition: 'center' });
    this.router.navigate(['login']);
  }
}
