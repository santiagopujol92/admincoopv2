export class Producto {
  Id_Producto: number;
  N_Producto: string;
  rubrosXProductos?: string;
  especificaciones: string;
  Precio_Anterior: number;
  Tipo_Precio_Anterior: string;
  Precio_Inicial: number;
  Tipo_Precio: string;
  Variacion: number;
  Id_Moneda: number;
  Id_Alicota_Iva: number;
  Bonificacion: number;
  Estado: string;
  Observaciones: string;
  atributosXProductos?: string;
  indicesXProductos?: string;

  constructor(precioInicial: number,
    nombreProducto: string,
    especifiaciones: string,
    atributosXProductos?: string,
    Id_Producto?: number,
    rubrosXProductos?: string,
    indicesXProductos?: string,
    precioAnterior?: number,
    tipoPrecioAnterior?: string,
    tipoPrecio?: string,
    variacion?: number,
    idMoneda?: number,
    idAlicuotaIva?: number,
    bonificacion?: number,
    estado?: string,
    observaciones?: string,
  ) {
    this.Id_Producto = Id_Producto;
    this.Precio_Inicial = precioInicial;
    this.N_Producto = nombreProducto;
    this.especificaciones = especifiaciones;
    this.atributosXProductos = atributosXProductos;
    this.rubrosXProductos = rubrosXProductos;
    this.indicesXProductos = indicesXProductos;
    this.Precio_Anterior = precioAnterior;
    this.Tipo_Precio_Anterior = tipoPrecioAnterior;
    this.Tipo_Precio = tipoPrecio;
    this.Variacion = variacion;
    this.Id_Moneda = idMoneda;
    this.Id_Alicota_Iva = idAlicuotaIva;
    this.Bonificacion = bonificacion;
    this.Estado = estado;
    this.Observaciones = observaciones;
  }
}
