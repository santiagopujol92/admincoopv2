export class Usuario {
    id?: number;
    email?: string;
    name?: string;
    lastname?: string;
    username?: string;
    password?: string;
    type_description?: string;
    type?: number;
    status?: string;
}
