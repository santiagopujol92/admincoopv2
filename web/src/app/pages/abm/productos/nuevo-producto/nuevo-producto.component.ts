import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, ErrorStateMatcher } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-nuevo-producto',
  templateUrl: './nuevo-producto.component.html',
  styleUrls: ['./nuevo-producto.component.css']
})
export class NuevoProductoComponent implements OnInit {
  myForm: FormGroup;
  producto: any;

  constructor(public dialogRefe: MatDialogRef<NuevoProductoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public fb: FormBuilder ) {
      this.producto = {};

      if (data.producto) {
        this.producto = data.producto;
        this.myForm = this.fb.group({
          codigo: [data.producto.Precio_Inicial, [Validators.required, Validators.maxLength(10)]],
          nombre: [data.producto.N_Producto, [Validators.required]],
          especificaciones: [data.producto.especificaciones, [Validators.required]],
        });
      } else {
        this.myForm = this.fb.group({
          codigo: ['', [Validators.required, Validators.maxLength(10)]],
          nombre: ['', [Validators.required]],
          especificaciones: ['', [Validators.required]],
        });
      }
     }

  ngOnInit() {
  }

  cerrar(): void {
    this.dialogRefe.close();
  }

}
