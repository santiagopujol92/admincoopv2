import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar} from '@angular/material';
import { NuevoProductoComponent } from './nuevo-producto/nuevo-producto.component';
import { ConfirmacionComponent } from 'src/app/shared/mensajes/confirmacion/confirmacion.component';
import { ProductoService } from 'src/app/services/productos.service';
import { Producto } from 'src/app/models/producto';

/* export interface Producto {
  codigo: number;
  nombre: string;
  precio?: Number;
  moneda?: string;
  iva?: string;
  especificaciones?: string;
} */

/* const ELEMENT_DATA: Producto[] = [
  { codigo: 1, nombre: 'FACTURAS Y CHURROS',  precio: 100, moneda: 'AR', iva: '21 %', especificaciones: 'Facturas. Medialunas de grasa o de manteca por docena' },
  { codigo: 2, nombre: 'GALLETAS, GRISINES Y TOSTADAS',  precio: 88, moneda: 'AR', iva: '21 %', especificaciones: 'Tostadas. Sueltas por 100g o Paquete de 200 a 400g. Comunes o Light.' },
  { codigo: 3, nombre: 'GALLETITAS DE AGUA ENVASADA',  precio: 29, moneda: 'AR', iva: '21 %', especificaciones: 'Paquete de 1 a 3 de 100g. A 400g. Origen Argentina' },
  { codigo: 4, nombre: 'GALLETITAS DULCES ENVASADAS',  precio: 11, moneda: 'AR', iva: '21 %', especificaciones: 'Paquete individual de 110 a 175 g. Con o sin relleno. Excluir Obleas o con agregados.' },
  { codigo: 5, nombre: 'PAN INTEGRAL FRESCO EN PIEZAS',  precio: 111, moneda: 'AR', iva: '21 %', especificaciones: 'Precio por Kg.' },
  { codigo: 6, nombre: 'PAN TIPO FRANCÉS FRESCO EN PIEZAS',  precio: 111, moneda: 'AR', iva: '21 %', especificaciones: 'Pan negro. Cualquier variedad, piezas chicas. Precio por kg.' },
  { codigo: 7, nombre: 'PAN RALLADO',  precio: 111, moneda: 'AR', iva: '21 %', especificaciones: 'Rebozador o Suelto- Excluir con aditamentos. Envase de 450 a 550g o precio por kilo. Origen Argentina' },
  { codigo: 8, nombre: 'PANES Y PANECILLOS FRESCOS DE OTRO TIPO',  precio: 111, moneda: 'AR', iva: '21 %', especificaciones: 'Pan criollo por kg. Tipo común u hojaldre. Suelto. ' },
  { codigo: 9, nombre: 'TORTAS, TARTAS, PAN DULCE',  precio: 111, moneda: 'AR', iva: '21 %', especificaciones: 'Pan criollo por kg. Tipo común u hojaldre. Suelto. ' },
  { codigo: 10, nombre: 'PANES Y PANECILLOS FRESCOS DE OTRO TIPO',  precio: 111, moneda: 'AR', iva: '21 %', especificaciones: 'Tarta dulce, tipo pasta frola o tarta de ricotta. Tamaño mediano de aprox 20 cm. Excluir tarteletas, por unidad. ' },
  { codigo: 11, nombre: 'Coca Cola',  precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 12, nombre: 'Monitor',  precio: 150, moneda: 'U$S', iva: '21 %' },
  { codigo: 13, nombre: 'Ravioles',  precio: 60, moneda: 'AR', iva: '21 %' },
  { codigo: 14, nombre: 'Yerba',   precio: 170, moneda: 'U$S', iva: '21 %' },
  { codigo: 15, nombre: 'Lapicera',  precio: 190, moneda: 'AR', iva: '21 %' },
  { codigo: 16, nombre: 'Cerveza',   precio: 88, moneda: 'AR', iva: '21 %' },
  { codigo: 17, nombre: 'Termo',  precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 18, nombre: 'Alcancia',  precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 19, nombre: 'Tijera',  precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 20, nombre: 'Peluca',   precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 21, nombre: 'Afeitadora',  precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 22, nombre: 'Celular',  precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 23, nombre: 'Pelota',  precio: 150, moneda: 'AR', iva: '21 %' },
  { codigo: 24, nombre: 'Reloj',  precio: 60, moneda: 'AR', iva: '21 %' },
  { codigo: 25, nombre: 'Taza',   precio: 170, moneda: 'AR', iva: '21 %' },
  { codigo: 26, nombre: 'Bolsa',  precio: 190, moneda: 'AR', iva: '21 %' },
  { codigo: 27, nombre: 'Tapa',   precio: 88, moneda: 'AR', iva: '21 %' },
  { codigo: 28, nombre: 'Cuchara',  precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 29, nombre: 'Tenedor',  precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 30, nombre: 'Traje',  precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 31, nombre: 'Heladera',   precio: 100, moneda: 'AR', iva: '21 %' },
  { codigo: 32, nombre: 'Agua Mineral',  precio: 100, moneda: 'AR', iva: '21 %' },
]; */

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
  codigo: number;
  nombreProducto: string;
  especificaciones: string;
  matSpinner = true;

  displayedColumns: string[] = ['Precio_Inicial', 'N_Producto', 'especificaciones', 'acciones'];
  dataSource: MatTableDataSource<Producto>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog, public _productoService: ProductoService, public snackBar: MatSnackBar) {

   /*  this.dataSource = new MatTableDataSource(ELEMENT_DATA); */
  }

  ngOnInit() {
  /*   this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort; */
    this.listarProductos();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(): void {
    const dialogRefe = this.dialog.open(NuevoProductoComponent, {
      width: '800px',
      data: {}
    });

    dialogRefe.afterClosed().subscribe(result => {
      if (result === undefined) {
        return ;
      }
      const producto: Producto = new Producto(result.codigo, result.nombre, result.especificaciones);
      this._productoService.insertarProducto(producto).subscribe(data => {
        this.listarProductos();
      });
      console.log(result);
    });
  }

  eliminarProducto(idProducto) {
    const dialogConfirmDelete = this.dialog.open(ConfirmacionComponent, {
      width: '400px',
      data: { titulo: 'Esperee! ¿Realmente desea eliminar?', texto: '¿Está seguro que desea eliminar el producto seleccionado?' }
    });
    dialogConfirmDelete.afterClosed().subscribe(result => {

      if (result === 'SI') {
        this._productoService.eliminarProducto(idProducto).subscribe(data => {
          this.listarProductos();
          this.snackBar.open('El producto fue eliminado con exito!', 'Cerrar', {
            duration: 3000,
          });
        });
      }
    });
  }


  // asociarAtributos(idProducto): void {
  //   console.log(idProducto);

  //   const dialogRef = this.dialog.open(AsociarAtributosComponent, {
  //     width: '800px',
  //     data: { idProducto }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //   });
  // }

  // asociarRubros(idProducto): void {
  //   const dialogRef = this.dialog.open(AsociarRubrosComponent, {
  //     width: '800px',
  //     data: { idProducto }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //   });
  // }

  // asociarIndices(idProducto): void {
  //   const dialogRef = this.dialog.open(AsociarIndicesComponent, {
  //     width: '800px',
  //     data: { idProducto }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //   });
  // }

  listarProductos() {
    this.matSpinner = true;
    this._productoService.obtenerListaProductos().subscribe(data => {
        console.log(data);
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.matSpinner = false;
    },
    err => {
      this.matSpinner = false;
      this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
        duration: 7000,
      });
    });
  }

  editarProducto(element: any) {
    const dialogRefe = this.dialog.open(NuevoProductoComponent, {
      width: '800px',
      data: {producto: element}
    });

    dialogRefe.afterClosed().subscribe(result => {
      if (result === undefined) {
        return ;
      }
      const producto: Producto = new Producto(result.codigo, result.nombre, result.especificaciones);
      this._productoService.editarProducto(producto).subscribe(data => {
        this.listarProductos();
      });
    });
  }
}

