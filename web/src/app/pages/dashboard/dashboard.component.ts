import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  prueba: string ;
  constructor(private _loginService: LoginService
    ) { }

  ngOnInit() {
    console.log(this._loginService.getUserLogged());
  }

}
