import { LOCALE_ID, NgModule } from '@angular/core';
import { PAGES_ROUTES } from './pages.routes';

import { SharedModule } from '../shared/shared.module';

import { PagesComponent } from './pages.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductosComponent } from './abm/productos/productos.component';
import { NuevoProductoComponent } from './abm/productos/nuevo-producto/nuevo-producto.component';
import { registerLocaleData, CommonModule } from '@angular/common';
import { HojaDeRutaComponent } from './hoja-de-ruta/hoja-de-ruta.component';
import { TipoNegocioPipe, EstadoInformantePipe, BaseRefPipe, MedioCaptacionPipe, SafePipe } from './../pipes/enumeradores.pipe';
import { CuilPipe, RemoverGuionesPipe } from '../pipes/cuil.pipe';
import { NuevaHojaRutaComponent } from './hoja-de-ruta/nueva-hoja-ruta/nueva-hoja-ruta.component';
import { AgmCoreModule } from '@agm/core';
import { ModificarHojaDeRutaComponent } from './hoja-de-ruta/modificar-hoja-de-ruta/modificar-hoja-de-ruta.component';
import { VerCalendarizacionComponent } from './hoja-de-ruta/ver-calendarizacion/ver-calendarizacion.component';
import localeDeAt from '@angular/common/locales/es-AR';
import { ReasignarHojaDeRutaComponent } from './hoja-de-ruta/reasignar-hoja-de-ruta/reasignar-hoja-de-ruta.component';
import { ReasignarEncuestadorComponent } from './hoja-de-ruta/reasignar-encuestador/reasignar-encuestador.component';
import { GetFirstWord } from '../pipes/firstWord';

registerLocaleData(localeDeAt);

@NgModule({
    declarations: [
        PagesComponent,
        DashboardComponent,
        ProductosComponent,
        NuevoProductoComponent,
        HojaDeRutaComponent,
        TipoNegocioPipe,
        EstadoInformantePipe,
        BaseRefPipe,
        MedioCaptacionPipe,
        SafePipe,
        CuilPipe,
        RemoverGuionesPipe,
        NuevaHojaRutaComponent,
        ModificarHojaDeRutaComponent,
        VerCalendarizacionComponent,
        ReasignarHojaDeRutaComponent,
        ReasignarEncuestadorComponent,
        GetFirstWord,
    ],
    providers: [{ provide: LOCALE_ID, useValue: 'es-AR' }],
    exports: [
        DashboardComponent,
    ],
    imports: [
        SharedModule,
        PAGES_ROUTES,
        CommonModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBSjYK6WRGJ-WHh9Cz3BGlGZXCGZ5DZ_tI'
        })
    ],
    entryComponents: [
        NuevoProductoComponent,
        VerCalendarizacionComponent,
        ReasignarEncuestadorComponent,
    ],
})
export class PagesModule { }
