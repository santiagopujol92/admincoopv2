import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductosComponent } from './abm/productos/productos.component';
import { NuevaHojaRutaComponent } from './hoja-de-ruta/nueva-hoja-ruta/nueva-hoja-ruta.component';
import { ModificarHojaDeRutaComponent } from './hoja-de-ruta/modificar-hoja-de-ruta/modificar-hoja-de-ruta.component';
import { ReasignarHojaDeRutaComponent } from './hoja-de-ruta/reasignar-hoja-de-ruta/reasignar-hoja-de-ruta.component';

const pagesRoutes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'productos', component: ProductosComponent },
            { path: 'nueva-hoja-ruta', component: NuevaHojaRutaComponent },
            { path: 'modificar-hoja-ruta/:id', component: ModificarHojaDeRutaComponent },
            { path: 'reasignar-hoja-de-ruta', component: ReasignarHojaDeRutaComponent },
            { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
        ]
    }
];


export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
