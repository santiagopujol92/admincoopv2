import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar, MatSelectionList } from '@angular/material';
import { UsuarioService } from '../../../services/usuarios.service';
import { HojaDeRutaService } from '../../../services/hoja-de-ruta.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmacionComponent } from '../../../shared/mensajes/confirmacion/confirmacion.component';
import { InformacionComponent } from '../../../shared/mensajes/informacion/informacion.component';

@Component({
  selector: 'app-reasignar-hoja-de-ruta',
  templateUrl: './reasignar-hoja-de-ruta.component.html',
  styleUrls: ['./reasignar-hoja-de-ruta.component.css']
})

export class ReasignarHojaDeRutaComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('informantes_a_reasignar') lst_informantes: MatSelectionList;

  camera: any = { lat: -31.414911, lng: -64.183696 };
  camera_zoom: Number = 13;

  matSpinner = true;

  lat: Number = -31.407702;
  lng: Number = -64.179264;

  lst_encuestadores = [];
  lst_rubros = [];
  lst_tipos_negocio_informante = [];
  lst_informantes_total = [];
  lst_informantes_a_reasignar = [];
  lst_informantes_seleccionados_a_reasignar = [];
  lst_informantes_seleccionados_a_quitar = [];
  displayedColumns = ['Referencia', 'acciones'];
  lst_hojas_rutas = [];
  lst_hojas_rutas_generadas = [];
  lst_referencias_base_informante = [];
  lst_medios_captacion = [];
  selectHRaReasignar = null;

  // Lista de Iconos
  lst_icons = [{ Nombre: '../../../assets/img/icons/Pmarron.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pmorado.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pnaranjaclaro.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Projo.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pnegro.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Prosa.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pverde.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pverdeclaro.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pvioleta.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Plila.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pamarillo.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pbadge.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pceleste.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pcelesteoscuro.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pgris.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pazulclaro.png', Height: 40, Width: 40 },
  { Nombre: '../../../assets/img/icons/Pazuloscuro.png', Height: 40, Width: 40 }
  ];

  dataSource = null;
  // filtro = new Object();
  data_a_reasignar = { Id_Planificacion: null, Id_Hoja_Ruta: null, idsInformantes: '', N_HojaDeRuta: '' };
  cantidad_productos_a_reasignar = 0;
  openedWindowInMarker = 0;
  inputNHojaRuta = null;
  ocultarDivInputNHojaRuta = true;

  constructor(public dialog: MatDialog,
    // public informantesService: InformantesService,
    public snackBar: MatSnackBar,
    private usuarioService: UsuarioService,
    private hojaDeRutaService: HojaDeRutaService,
    private route: Router,
  ) {
  }

  ngOnInit() {
    // this.cargarInformantesActivosYHojasDeRuta();
    this.cargarHojasRutasToReasignar();
    // this.cargarRubros();
    // this.cargarTiposNegocioInformante();
    // this.cargarReferenciasBaseInformante();
    // this.cargarMediosCaptacion();
    this.cargarEncuestadores();
  }

  cargarEncuestadores() {
    this.usuarioService.obtenerListaEncuestadores().subscribe(data => {
      this.lst_encuestadores.push({ Id_Usuario: '', Apellido: '', Nombre: 'TODOS' });
      data.forEach(item => {
        this.lst_encuestadores.push(item);
      });
      this.matSpinner = false;
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
      });
  }

  // cargarInformantesActivosYHojasDeRuta() {
  //   this.matSpinner = true;
  //   this.informantesService.obtenerListaInformantesActivosPorHR().subscribe(data => {

  //     this.lst_hojas_rutas.push({ 'Id_Hoja_Ruta': '', 'N_Hoja_Ruta': 'TODAS' });
  //     let id_icon_por_HR = 0;

  //     // Leo la data para configurar los iconos y obtener las hojas de rutas distinguidas
  //     data.forEach(item => {
  //       // Si no encuentra el icono reinicio el identificador de iconos
  //       id_icon_por_HR = (this.lst_icons[id_icon_por_HR] == null) ? 0 : id_icon_por_HR;

  //       // Cargar Array las hojas de rutas distinguidas del listado e iconos
  //       const hoja_de_ruta_existente = this.lst_hojas_rutas.find(p => p.Id_Hoja_Ruta === item.Id_Hoja_Ruta);
  //       if (!hoja_de_ruta_existente) {
  //         // Si no es una hoja de ruta repetida o existente en el array de lst_hojas_rutas guardo en la lista de hojas de ruta distinguida
  //         this.lst_hojas_rutas.push({ 'Id_Hoja_Ruta': item.Id_Hoja_Ruta, 'Id_Planificacion': item.Id_Planificacion, 'N_Hoja_Ruta': item.N_Hoja_Ruta, 'Id_Estado_PL': item.Id_Estado_PL });

  //         // Cargo em la propiedad icon el objeto icon por hoja de ruta varia con el id y le asigno el siguiente icono al proximo
  //         item.Icon = this.lst_icons[id_icon_por_HR];
  //         id_icon_por_HR++;
  //       } else {
  //         // Si ya existe uso el icono anterior
  //         item.Icon = this.lst_icons[id_icon_por_HR - 1];
  //       }
  //     });

  //     // Cargo el data ya con los agregados en el datasource
  //     this.lst_informantes_total = data;
  //     this.dataSource = new MatTableDataSource(data);
  //     this.dataSource.paginator = this.paginator;
  //     this.dataSource.sort = this.sort;
  //     // this.matSpinner = false;
  //     this.applyFilter();
  //   },
  //     err => {
  //       this.matSpinner = false;
  //       this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
  //         duration: 7000,
  //       });
  //     });
  // }

  // cargarRubros() {
  //   this.rubroService.obtenerListaRubros().subscribe((data) => {
  //     this.lst_rubros.push({ Id_Rubro: '', N_Rubro: 'TODOS' });
  //     data.forEach(item_rubro => {
  //       this.lst_rubros.push(item_rubro);
  //     });
  //   },
  //     err => {
  //       this.matSpinner = false;
  //       this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
  //         duration: 7000,
  //       });
  //     });
  // }

  // cargarReferenciasBaseInformante() {
  //   this.informantesService.obtenerListaOrigenesInformante().subscribe((data) => {
  //     this.lst_referencias_base_informante.push({ Id_Origen_Inf: '', N_Origen_Inf: 'TODOS' });
  //     data.forEach(item_referencias_base_informante => {
  //       this.lst_referencias_base_informante.push(item_referencias_base_informante);
  //     });
  //   },
  //     err => {
  //       this.matSpinner = false;
  //       this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
  //         duration: 7000,
  //       });
  //     });
  // }

  // cargarMediosCaptacion() {
  //   this.informantesService.obtenerListaMediosCaptacionInformante().subscribe((data) => {
  //     this.lst_medios_captacion.push({ Id_Medio_Captacion: '', N_Medio_Captacion: 'TODOS' });
  //     data.forEach(item_medio_captacion => {
  //       this.lst_medios_captacion.push(item_medio_captacion);
  //     });
  //     this.matSpinner = false;
  //   },
  //     err => {
  //       this.matSpinner = false;
  //       this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
  //         duration: 7000,
  //       });
  //     });
  // }

  // cargarTiposNegocioInformante() {
  //   this.informantesService.obtenerListaTiposNegocioInformante().subscribe((data) => {
  //     this.lst_tipos_negocio_informante.push({ Id_Tipo_Inf: '', N_Tipo_Inf: 'TODOS' });
  //     data.forEach(item_tipo_negocio_informante => {
  //       this.lst_tipos_negocio_informante.push(item_tipo_negocio_informante);
  //     });
  //   },
  //     err => {
  //       this.matSpinner = false;
  //       this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
  //         duration: 7000,
  //       });
  //     });
  // }

  agregarInformantesAReasignar() {
    if (this.lst_informantes_seleccionados_a_reasignar.length > 0) {
      this.lst_informantes_seleccionados_a_reasignar.forEach(item_to_remove => {
        for (let index = 0; index < this.lst_informantes_total.length; index++) {
          if (item_to_remove.Id_Informante === this.lst_informantes_total[index].Id_Informante) {
            this.lst_informantes_a_reasignar.push(this.lst_informantes_total[index]);
            this.dataSource.data.splice(index, 1);
          }
        }
      });
      // this.applyFilter();
      this.CalcularProductos();
      this.snackBar.open('Informantes Preparados para Reasignar Correctamente', 'Cerrar', {
        duration: 3000,
      });
    } else {
      this.snackBar.open('No hay informnates Seleccionados para Reasignar', 'Cerrar', {
        duration: 3000,
      });
    }
  }

  quitarInformantesAReasignar() {
    if (this.lst_informantes_seleccionados_a_quitar.length > 0) {
      this.lst_informantes_seleccionados_a_quitar.forEach(item_to_remove => {
        for (let index = 0; index < this.lst_informantes_a_reasignar.length; index++) {
          if (item_to_remove.Id_Informante === this.lst_informantes_a_reasignar[index].Id_Informante) {
            this.lst_informantes_total.push(this.lst_informantes_a_reasignar[index]);
            this.lst_informantes_a_reasignar.splice(index, 1);
          }
        }
      });
      // this.applyFilter();
      this.CalcularProductos();
      this.snackBar.open('Informantes Quitados para Reasignar Correctamente', 'Cerrar', {
        duration: 3000,
      });
    } else {
      this.snackBar.open('No hay informnates Seleccionados para Quitar', 'Cerrar', {
        duration: 3000,
      });
    }
  }

  // filterByRubro(filterValue: string) {
  //   this.filtro.Rubro = filterValue;
  //   this.applyFilter();
  // }

  // filterByTipoNegocio(filterValue: string) {
  //   this.filtro.Id_Tipo_Informante = filterValue;
  //   this.applyFilter();
  // }

  // filterByEncuestador(filterValue: string) {
  //   this.filtro.Id_Encuestador = filterValue;
  //   this.applyFilter();
  // }

  // filterByIdHojaDeRuta(filterValue: string) {
  //   this.filtro.Id_Hoja_Ruta = filterValue;
  //   this.applyFilter();
  // }

  // filterByBase(filterValue: string) {
  //   this.filtro.Id_Base = filterValue;
  //   this.applyFilter();
  // }

  // filterByCaptacion(filterValue: string) {
  //   this.filtro.Id_Medio_Captacion = filterValue;
  //   this.applyFilter();
  // }

  // getFilterString(): string {
  //   let result = '';
  //   result += this.filtro.Rubro;
  //   result += ';' + this.filtro.Id_Tipo_Informante;
  //   result += ';' + this.filtro.Id_Encuestador;
  //   result += ';' + this.filtro.Id_Hoja_Ruta;
  //   result += ';' + this.filtro.Id_Base;
  //   result += ';' + this.filtro.Id_Medio_Captacion;
  //   return result;
  // }

  // applyFilter() {
  //   const filterValue = this.getFilterString().trim().toLowerCase();
  //   this.dataSource.filterPredicate = function (data, filter: string): boolean {
  //     return true
  //       && InformantesComponent.existInArrayFilter(filter, 0, data.RubrosAsignados)
  //       && InformantesComponent.equalsFilter(filter, 1, data.Id_Tipo_Informante)
  //       && InformantesComponent.equalsFilter(filter, 2, data.Id_Encuestador)
  //       && InformantesComponent.equalsFilter(filter, 3, data.Id_Hoja_Ruta)
  //       && InformantesComponent.equalsFilter(filter, 4, data.Id_Base)
  //       && InformantesComponent.equalsFilter(filter, 5, data.Id_Medio_Captacion);
  //   };

  //   this.dataSource.filter = filterValue;
  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  //   this.animarSeleccionado(null);
  // }

  animarSeleccionado(informante_seleccionado) {
    this.lst_informantes_a_reasignar.forEach(element => {
      element.animacion = null;
    });

    this.dataSource.data.forEach(element => {
      element.animacion = null;
    });
    setTimeout(() => {
      if (informante_seleccionado) {
        informante_seleccionado.animacion = 'BOUNCE';
      }
    }, 500);
  }

  ubicarInformanteEnMapa(informante_seleccionado) {
    if (informante_seleccionado) {
      this.camera = { lat: Number(informante_seleccionado.Latitud), lng: Number(informante_seleccionado.Longitud) };
      this.ubicarInformanteEnLista(informante_seleccionado);
      this.openWindow(informante_seleccionado.Id_Informante);
    }
  }

  ubicarInformanteEnLista(informante_seleccionado) {
    this.camera = { lat: Number(informante_seleccionado.Latitud), lng: Number(informante_seleccionado.Longitud) };
    this.animarSeleccionado(informante_seleccionado);
    this.lst_informantes.options.forEach(i_option => {
      if (i_option.value.Id_Informante.toString() === informante_seleccionado.Id_Informante.toString()) {
        i_option.focus();
      }
    });
  }

  confirmarGuardar() {
    if (this.selectHRaReasignar != null) {
      const dialogConfirmDelete = this.dialog.open(ConfirmacionComponent, {
        width: '400px',
        data: { titulo: '¿Realmente desea aplicar la Reasignación de Hoja de Ruta?' }
      });
      dialogConfirmDelete.afterClosed().subscribe(result => {
        if (result === 'SI') {

          let idsInformantes = '';
          this.lst_informantes_a_reasignar.forEach(element => {
            idsInformantes += ',' + element.Id_Informante;
          });
          idsInformantes = idsInformantes.substring(1);

          this.data_a_reasignar.idsInformantes = idsInformantes;
          this.data_a_reasignar.Id_Planificacion = this.selectHRaReasignar.Id_Planificacion;
          this.data_a_reasignar.Id_Hoja_Ruta = this.selectHRaReasignar.Id_Hoja_Ruta;

          // Si la hoja de ruta es 0 llamo al proceso que reasigna a nueva hoja de ruta
          if (this.data_a_reasignar.Id_Hoja_Ruta === '0') {
            this.guardarReasignarInformantesNuevaHojaDeRuta();
          } else {
          // Si no es 0 llamo al proceso normal de reasignacion de hoja de ruta
            this.guardarReasignarInformantesHojaDeRuta();
          }
        }
      });
    }
  }

  guardarReasignarInformantesHojaDeRuta() {
    this.matSpinner = true;
    this.hojaDeRutaService.reasignarInformantesHojaDeRuta(this.data_a_reasignar).subscribe(data => {
      this.snackBar.open('La Hoja de Ruta ha sido reasignada con éxito!', 'Cerrar', {
        duration: 3000,
      });
      this.matSpinner = false;
      // Si no viene -1 fue con exito
      if (data.p_Id !== -1) {
        this.actualizarDatosPantalla();
      // Si el data.p_Id viene -1 entonces significa que esos informantes no se pueden reasignar, entonces muestro mensaje
      } else {
        this.snackBar.open('No se puede realizar la reasignación: ' + data.p_Error, 'Cerrar', {
          duration: 7000,
        });
      }
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
      });
  }

  guardarReasignarInformantesNuevaHojaDeRuta() {
    this.matSpinner = true;
    this.hojaDeRutaService.reasignarInformantesNuevaHojaDeRuta(this.data_a_reasignar).subscribe(data => {
      console.log(data);
      // Si es -2 es porque la hoja de ruta ya existe
      if (data.p_Id === -2) {
        this.dialog.open(InformacionComponent, {
          width: '400px',
          data: { titulo: 'Atención !!', texto: 'El nombre de la Hoja de Ruta ya existe' }
        });
        this.data_a_reasignar.N_HojaDeRuta = '';
      // Si no guarda normal
      } else {
        this.snackBar.open('Se reasignaron informantes a una Nueva Hoja de Ruta con éxito!', 'Cerrar', {
          duration: 3000,
        });

        this.matSpinner = false;
        // Si no viene -1 fue con exito
        if (data.p_Id !== -1) {
          this.actualizarDatosPantalla();
        // Si el data.p_Id viene -1 entonces significa que esos informantes no se pueden reasignar, entonces muestro mensaje
        } else {
          this.snackBar.open('No se puede realizar la reasignación: ' + data.p_Error, 'Cerrar', {
            duration: 7000,
          });
        }
      }
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
      });
  }

  actualizarDatosPantalla() {
      // Reinicio todo y renderizo
      this.lst_informantes_total = [];
      this.lst_informantes_a_reasignar = [];
      this.lst_informantes_seleccionados_a_reasignar = [];
      this.lst_informantes_seleccionados_a_quitar = [];
      this.lst_hojas_rutas = [];
      this.lst_hojas_rutas_generadas = [];
      this.selectHRaReasignar = null;
      this.data_a_reasignar = { Id_Planificacion: null, Id_Hoja_Ruta: null, idsInformantes: '', N_HojaDeRuta: '' };
      this.ocultarDivInputNHojaRuta = true;

      // this.cargarInformantesActivosYHojasDeRuta();
      this.cargarHojasRutasToReasignar();
  }

  cancelar() {
    const dialogConfirmDelete = this.dialog.open(ConfirmacionComponent, {
      width: '400px',
      data: { titulo: '¿Está seguro que desea cancelar?' }
    });
    dialogConfirmDelete.afterClosed().subscribe(result => {
      if (result === 'SI') {
        this.route.navigateByUrl('\hoja-de-ruta');
      }
    });
  }

  CalcularProductos() {
    let idsInformantes = '';
    this.lst_informantes_a_reasignar.forEach(element => {
      idsInformantes += ',' + element.Id_Informante;
    });
    idsInformantes = idsInformantes.substring(1);
    if (idsInformantes.length > 0) {
      // this.matSpinner = true;
      // this.informantesService.obtenerCantidadDeProductosByInformantes(idsInformantes).subscribe(data => {
      //   this.cantidad_productos_a_reasignar = data;
      //   this.matSpinner = false;
      // },
      //   err => {
      //     this.matSpinner = false;
      //     this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
      //       duration: 7000,
      //     });
      //   });
    } else {
      this.cantidad_productos_a_reasignar = 0;
    }

  }

  openWindow(id) {
    this.openedWindowInMarker = id;
  }

  isInfoWindowOpen(id) {
    return this.openedWindowInMarker === id;
  }

  cargarHojasRutasToReasignar() {
    this.lst_hojas_rutas_generadas.push({ 'Id_Hoja_Ruta': '0', 'N_HojaDeRuta': 'NUEVA' });
    this.hojaDeRutaService.obtenerListaHojasDeRutaGeneradas().subscribe((data) => {
      data.forEach(item => {
        this.lst_hojas_rutas_generadas.push(item);
      });
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
      });
  }

  changeSelectHRaReasignar(valueSelected) {
    this.ocultarDivInputNHojaRuta = true;
    if (valueSelected.Id_Hoja_Ruta === '0') {
      this.ocultarDivInputNHojaRuta = false;
    }
  }

  concatIdHojaRuta(value) {
    if (value === '0') {
      return '';
    } else if (value !== '' ) {
      return '- ' + value;
    }
    return value;
  }
}
