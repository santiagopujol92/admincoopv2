import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatTable, MatSnackBar, MatDatepickerInputEvent } from '@angular/material';
import { HojaDeRutaService } from 'src/app/services/hoja-de-ruta.service';
import { VerCalendarizacionComponent } from './ver-calendarizacion/ver-calendarizacion.component';
import { ConfirmacionComponent } from '../../shared/mensajes/confirmacion/confirmacion.component';
import { ReasignarEncuestadorComponent } from './reasignar-encuestador/reasignar-encuestador.component';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';

export interface PeriodicElement {
  encuestador: string;
  cantProd: number;
  cantInfo: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { encuestador: 'Nicolas', cantProd: 125, cantInfo: 5 },
  { encuestador: 'Tomas', cantProd: 250, cantInfo: 10 },
  { encuestador: 'Federico', cantProd: 300, cantInfo: 8 },
  { encuestador: 'Pedro', cantProd: 130, cantInfo: 11 },
  { encuestador: 'Gina', cantProd: 88, cantInfo: 7 },
  { encuestador: 'Marcelo', cantProd: 166, cantInfo: 12 },
];

@Component({
  selector: 'app-hoja-de-ruta',
  templateUrl: './hoja-de-ruta.component.html',
  styleUrls: ['./hoja-de-ruta.component.css']
})
export class HojaDeRutaComponent implements OnInit {
  displayedColumns1: string[] = ['N_Encuestador', 'C_Informantes', 'C_Productos'];
  dataSource2;
  selectedCheckHojaDeRuta = [];
  displayedColumns = ['Id_Hoja_Ruta', 'N_Nombre', 'C_Informantes', 'C_Productos', 'N_Encuestador', 'fecha', 'Estado', 'acciones'];
  dataSource = new MatTableDataSource();
  arrayListado: any[] = [];
  matSpinner = true;
  mesesAnios = [];
  periodoActual = '';
  fechaCalendarizacion = '';
  datepicker = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<IHojaDeRuta>;

  encuestadores: IEncuestador[];
  constructor(public dialog: MatDialog,
              private _loginService: LoginService,
              public _serviceHojaDeRuta: HojaDeRutaService,
              public snackBar: MatSnackBar,
              private router: Router) {

  }

  ngOnInit() {
    if (this._loginService.getUserLogged() === null) {
      this.router.navigateByUrl('\login');
      this.snackBar.open('La sesión está desconectada, debe autentificarse', '', { duration: 2500, panelClass: ['danger-snackbar'], verticalPosition: 'top', horizontalPosition: 'center' });
    }
    // this.cargarMesAnio();
    // this.cargarHojasDeRuta();
  }

  cargarHojasDeRuta() {
    this.encuestadores = [];
    this._serviceHojaDeRuta.obtenerListadoHojaRuta().subscribe(data => {

      this.arrayListado = data;
      this.obtenerEncuestadores(this.arrayListado, '');
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.matSpinner = false;
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
          panelClass: ['blue-snackbar'],
          verticalPosition: 'top', horizontalPosition: 'end'

        });

      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }

  }

  cargarSeleccion(element, checked) {
    if (checked) {
      this.selectedCheckHojaDeRuta.push(element);
    } else if (!checked) {
      // Buscar dentro del array filtrando por element.Id o algun identificador,
      // luego de eencontrar hacer un remove del elemento
      const hojaDeRutaDeseleccionada = this.selectedCheckHojaDeRuta.find(obj => {
        return obj.Id_Hoja_Ruta === element.Id_Hoja_Ruta;
      });
      this.selectedCheckHojaDeRuta.splice(this.selectedCheckHojaDeRuta.indexOf(hojaDeRutaDeseleccionada), 1);
    }
  }

  asignarEncuestador() {
    let listIdPlanificacion: any;
    listIdPlanificacion = '';
    this.selectedCheckHojaDeRuta.forEach(function (value) {
      listIdPlanificacion += ',' + value.Id_Planificacion;
    });
    listIdPlanificacion = listIdPlanificacion.substring(1);

    const dialogConfirmDelete = this.dialog.open(ConfirmacionComponent, {
      width: '400px',
      data: { titulo: 'Mensaje de Confirmación', texto: 'Las hojas de ruta seleccionadas y sus respectivas encuestas serán asignadas al encuestador. ¿Está seguro que desea confirmar?' }
    });
    dialogConfirmDelete.afterClosed().subscribe(result => {
      if (result === 'SI') {
        this.matSpinner = true;
        this._serviceHojaDeRuta.asignarHojaDeRutaAEncuestador(listIdPlanificacion).subscribe(data => {
          this.matSpinner = false;
          this.snackBar.open('Las hojas de ruta y sus encuestas han sido asignadas con éxito!', 'Cerrar', {
            duration: 7000,
          });
          this.cargarHojasDeRuta();
        });
      }
    });
  }
  verCalendarizacion(): void {
    const fecha = this.fechaCalendarizacion;
    const dialogRef = this.dialog.open(VerCalendarizacionComponent, {
      width: '1200px',
      data: { fecha }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) {
        return;
      }
    });
  }

  obtenerEncuestadores(listado, fecha) {
    this.encuestadores = [];
    listado.forEach(element => {
      if (element.N_Encuestador !== null) {
        const enc = {
          N_Encuestador: element.N_Encuestador,
          C_Informantes: 0,
          C_Productos: 0
        };
        this.encuestadores.push(enc);
      }
    });

    const eliminarRepetidos = (encuestadores) => {
      const unicos = [];
      const itemsEncontrados = {};
      for (let i = 0, l = encuestadores.length; i < l; i++) {
        const stringified = JSON.stringify(encuestadores[i]);
        if (itemsEncontrados[stringified]) { continue; }
        unicos.push(encuestadores[i]);
        itemsEncontrados[stringified] = true;
      }
      return unicos;
    };

    this.sumatoriaCantInfProd(listado, fecha);
    this.encuestadores = eliminarRepetidos(this.encuestadores);
    this.dataSource2 = this.encuestadores;
  }

  sumatoriaCantInfProd(listado, fecha) {
    if (fecha === null || fecha === '') {
      listado.forEach(element => {
        this.encuestadores.forEach(element2 => {
          if (element.N_Encuestador === element2.N_Encuestador) {
            element2.C_Informantes = element2.C_Informantes + Number(element.C_Informantes);
            element2.C_Productos = element2.C_Productos + Number(element.C_Productos);
          }
        });
      });
    } else {
      fecha = fecha.toLocaleDateString();
      listado.forEach(element => {
        this.encuestadores.forEach(element2 => {
          const parseFech = new Date(element.fecha);
          if (element.N_Encuestador === element2.N_Encuestador && parseFech.toLocaleDateString() === fecha ) {
            element2.C_Informantes = element2.C_Informantes + Number(element.C_Informantes);
            element2.C_Productos = element2.C_Productos + Number(element.C_Productos);
          }
        });
      });
    }

  }

  generarHojaDeRuta() {
    // Se le pasa en duro el id del usuario(en este caso nicolas
    const idUsuario = 38;

    this.dataSource = new MatTableDataSource(null);
    this._serviceHojaDeRuta.generacionHojaDeRuta(idUsuario).subscribe(resultado => {
      // En el caso que ya hayan generadas hojas de ruta para el mes en curso.
      if (resultado === 1) {
        this.cargarHojasDeRuta();
        this.cargarMesAnio();
        this.snackBar.open('La hoja de ruta se generó con éxito!', 'Cerrar', {
          duration: 7000,
        });
      } else if (resultado === 0) {
        this.cargarHojasDeRuta();
        this.snackBar.open('Ya existe una hoja de ruta generada para el mes siguiente', 'Cerrar', {
          duration: 7000,
        });
      } else {
        this.snackBar.open('Ha ocurrido un error', 'Cerrar', {
          duration: 7000,
        });
      }
    });
  }

  cargarMesAnio() {
    this.mesesAnios = [];
    this._serviceHojaDeRuta.cargarMesAño().subscribe(data => {
      data.forEach(item => {
        if (item.Fecha_String) {
          const arrayFecha = item.Fecha_String.split('/');
          const fecha = arrayFecha[0] + '/' + '01/' + arrayFecha[1];
          item.FechaDisplay = (new Date(fecha).toLocaleDateString('sp-US', { month: 'long' })).toUpperCase() + ' ' + arrayFecha[1];
          this.mesesAnios.push(item);
        }
      });

      const fechaActual = new Date();
      const anioActual = fechaActual.getFullYear();
      const mesActual = fechaActual.toLocaleDateString('sp-AR', { month: 'long'}).toUpperCase();
      this.periodoActual = mesActual + ' ' + anioActual;
    });
  }

  obtenerListadoHRByMesAnio(fecha) {
    const arrayFecha = fecha.split('/');
    this.fechaCalendarizacion = arrayFecha;
    const mes = arrayFecha[0];
    const anio = arrayFecha[1];
    this._serviceHojaDeRuta.obtenerListadoHRByMesesAnios(mes + '-' + anio).subscribe(data => {

      if (data.length > 0) {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.matSpinner = false;
        this.arrayListado = data;
        this.obtenerEncuestadores(this.arrayListado, '');
        this.datepicker = '';
      } else if (data.length === 0) {
        this.snackBar.open('No existen datos para el mes seleccionado', 'Cerrar', {
          duration: 7000,
        });
      }
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
      });
  }

  reasignarEncuestador(element): void {

    const dialogRef = this.dialog.open(ReasignarEncuestadorComponent, {
      width: '800px',
      data: {element}
    });
    dialogRef.afterClosed().subscribe(result => {

      if (result === undefined) {
        return;
      } else {
        this.matSpinner = true;
        this._serviceHojaDeRuta.reasignarEncuestador(result).subscribe(data => {
          this.snackBar.open('La hoja de ruta fue reasinada con exito!', 'Cerrar', {
            duration: 3000,
          });
          this.cargarHojasDeRuta();
        },
        err => {
          this.matSpinner = false;
          this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
            duration: 7000,
          });
        });
      }
      /* this.insertarAtributo(result.nombre); */
    });
  }

  changeDate(event: MatDatepickerInputEvent<Date>) {
    this.obtenerEncuestadores(this.arrayListado, event.value);
  }

  confirmarEliminarHojaRutaEInformantes(idPlanificacion) {
    const dialogConfirmDelete = this.dialog.open(ConfirmacionComponent, {
      width: '400px',
      data: { titulo: '¿Realmente desea dar de baja la Hoja de Ruta e Inactivar sus Informantes?' }
    });
    dialogConfirmDelete.afterClosed().subscribe(result => {
      if (result === 'SI') {
        this.BajaHojaDeRutaEInformantes(idPlanificacion);
      }
    });
  }

  BajaHojaDeRutaEInformantes(idPlanificacion) {
    this.matSpinner = true;
    this._serviceHojaDeRuta.bajaHojaRutaEInformantes(idPlanificacion, 2).subscribe(data => {
      this.snackBar.open('La Hoja de Ruta ha sido dada de baja con éxito!', 'Cerrar', {
        duration: 3000,
      });
      this.selectedCheckHojaDeRuta = [];
      this.dataSource = new MatTableDataSource();
      this.arrayListado = [];
      this.cargarHojasDeRuta();
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
    });
  }

  openSnack() {
    this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
      duration: 7000,
      panelClass: ['primary-snackbar'],
      verticalPosition: 'bottom', horizontalPosition: 'center'

    });
  }
  openSnack1() {
    this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
      duration: 7000,
      panelClass: ['success-snackbar'],
      verticalPosition: 'top', horizontalPosition: 'center'
    });
  }
  openSnack2() {
    this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
      duration: 7000,
      panelClass: ['info-snackbar'],
      verticalPosition: 'top', horizontalPosition: 'end'

    });
  }
  openSnack3() {
    this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
      duration: 7000,
      panelClass: ['danger-snackbar'],
      verticalPosition: 'bottom', horizontalPosition: 'end'

    });
  }
  openSnack4() {
    this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
      duration: 7000,
      panelClass: ['warning-snackbar'],
      verticalPosition: 'top', horizontalPosition: 'left'

    });
  }
    openSnack5() {
      this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
        duration: 7000,
        panelClass: ['light-snackbar'],
        verticalPosition: 'bottom', horizontalPosition: 'left'

      });
  }
}

export interface IEncuestador {
  N_Encuestador: string;
  C_Informantes: number;
  C_Productos: number;
}

export interface IHojaDeRuta {
  id: number;
}
