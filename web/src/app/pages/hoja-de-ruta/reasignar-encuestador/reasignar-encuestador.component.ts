import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { UsuarioService } from 'src/app/services/usuarios.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-reasignar-encuestador',
  templateUrl: './reasignar-encuestador.component.html',
  styleUrls: ['./reasignar-encuestador.component.css']
})
export class ReasignarEncuestadorComponent implements OnInit {
  hojaDeRuta: any;
  lst_encuestadores: any[];
  myForm: FormGroup;
  matSpinner = true;

  constructor(public dialogRef: MatDialogRef<ReasignarEncuestadorComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,
    public _serviceUsuarios: UsuarioService,
    public snackBar: MatSnackBar,
    public fb: FormBuilder
  ) {
    this.hojaDeRuta = data.element;
    this.lst_encuestadores = [];
  }

  ngOnInit() {
    this.obtenerEncuestadores();
  }

  obtenerEncuestadores() {
    this._serviceUsuarios.obtenerListaEncuestadores().subscribe(data => {
      this.lst_encuestadores = data;
      this.matSpinner = false;
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
      });
  }

  aceptar() {
    this.dialogRef.close(this.hojaDeRuta);
  }

  cerrar(): void {
    this.dialogRef.close();
  }

}
