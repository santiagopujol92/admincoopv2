import { Component, OnInit, ViewChild } from '@angular/core';
// import { InformantesService } from '../../../services/informantes.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar, MatSelectionList } from '@angular/material';
// import { FiltroInformante, InformantesComponent } from '../../abm/informantes/informantes.component';
import { UsuarioService } from '../../../services/usuarios.service';
import { HojaDeRutaService } from '../../../services/hoja-de-ruta.service';
import { Router } from '@angular/router';
import { ConfirmacionComponent } from '../../../shared/mensajes/confirmacion/confirmacion.component';
import { InformacionComponent } from 'src/app/shared/mensajes/informacion/informacion.component';

@Component({
  selector: 'app-nueva-hoja-ruta',
  templateUrl: './nueva-hoja-ruta.component.html',
  styleUrls: ['./nueva-hoja-ruta.component.css']
})
export class NuevaHojaRutaComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('informantes_inactivos') lst_informantes: MatSelectionList;

  camera: any = { lat: -31.414911, lng: -64.183696 };
  camera_zoom: Number = 13;

  matSpinner = true; // Se activa el loading

  lat: Number = -31.407702;
  lng: Number = -64.179264;

  lst_encuestadores = [];
  lst_rubros = [];
  lst_tipos_negocio_informante = [];
  lst_referencias_base_informante = [];
  lst_estados_informante = [];
  lst_medios_captacion = [];
  lst_informantes_asignados = [];
  lst_asignar = [];
  lst_desasignar = [];
  displayedColumns = ['Referencia', 'acciones'];

  nueva_hoja_ruta = { N_HojaDeRuta: '', Id_Encuestador: null, idsInformantes: '', fecha: null };

  dataSource = null;
  // filtro = new FiltroInformante();

  resaltar_informante_id = 0;
  cantidad_productos_asignados = 0;
  nueva_HojaDeRuta_Numero = '';
  openedWindowInMarker = 0;

  constructor(public dialog: MatDialog,
    // public informantesService: InformantesService,
    public snackBar: MatSnackBar,
    private usuarioService: UsuarioService,
    private hojaDeRutaService: HojaDeRutaService,
    private route: Router) {
    // this.filtro.Id_Estado_Informante = '1';
  }

  ngOnInit() {
    this.cargarInformantes();
    // this.cargarRubros();
    this.cargarTiposNegocioInformante();
    this.cargarReferenciasBaseInformante();
    // this.cargarEstadosInformante();
    this.cargarEncuestadores();
    this.cargarMediosCaptacion();
    this.cargarNumeroHojaRutaNueva(); // En este ultimo metodo se desactiva el loading
  }

  cargarEncuestadores() {
    this.usuarioService.obtenerListaEncuestadores().subscribe(data => {
      this.lst_encuestadores.push({ Id_Usuario: '0', Apellido: '', Nombre: 'SIN SELECCION' });
      data.forEach(item => {
        this.lst_encuestadores.push(item);
      });
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
    });
  }

  cargarInformantes() {
    // this.informantesService.obtenerListaInformantes().subscribe(data => {
    // this.dataSource = new MatTableDataSource(data);
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    // // this.matSpinner = false;
    // this.asignarMarkers();
    // this.filterByEstado('2');
    // this.applyFilter();
    // },
    //   err => {
    //     this.matSpinner = false;
    //     this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
    //       duration: 7000,
    //     });
    // });
  }

  // cargarRubros() {
  //   this.rubroService.obtenerListaRubros().subscribe((data) => {
  //     this.lst_rubros.push({ Id_Rubro: '', N_Rubro: 'TODOS' });
  //     data.forEach(item_rubro => {
  //       this.lst_rubros.push(item_rubro);
  //     });
  //   },
  //     err => {
  //       this.matSpinner = false;
  //       this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
  //         duration: 7000,
  //       });
  //   });
  // }

  cargarReferenciasBaseInformante() {
    // this.informantesService.obtenerListaOrigenesInformante().subscribe((data) => {
    //   this.lst_referencias_base_informante.push({ Id_Origen_Inf: '', N_Origen_Inf: 'TODOS' });
    //   data.forEach(item_referencias_base_informante => {
    //     this.lst_referencias_base_informante.push(item_referencias_base_informante);
    //   });
    // },
    //   err => {
    //     this.matSpinner = false;
    //     this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
    //       duration: 7000,
    //     });
    // });
  }

  cargarTiposNegocioInformante() {
    // this.informantesService.obtenerListaTiposNegocioInformante().subscribe((data) => {
    //   this.lst_tipos_negocio_informante.push({ Id_Tipo_Inf: '', N_Tipo_Inf: 'TODOS' });
    //   data.forEach(item_tipo_negocio_informante => {
    //     this.lst_tipos_negocio_informante.push(item_tipo_negocio_informante);
    //   });
    // },
    //   err => {
    //     this.matSpinner = false;
    //     this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
    //       duration: 7000,
    //     });
    // });
  }

  cargarEstadosInformante() {
    // this.informantesService.obtenerListaEstadosInformante().subscribe((data) => {
    //   this.lst_estados_informante.push({ Id_Estado_Inf: '', N_Estado_Inf: 'TODOS' });
    //   data.forEach(item_estado_informante => {
    //     this.lst_estados_informante.push(item_estado_informante);
    //   });
    // },
    //   err => {
    //     this.matSpinner = false;
    //     this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
    //       duration: 7000,
    //     });
    // });
  }

  cargarMediosCaptacion() {
    // this.informantesService.obtenerListaMediosCaptacionInformante().subscribe((data) => {
    //   this.lst_medios_captacion.push({ Id_Medio_Captacion: '', N_Medio_Captacion: 'TODOS' });
    //   data.forEach(item_medio_captacion => {
    //       this.lst_medios_captacion.push(item_medio_captacion);
    //   });
    //   this.matSpinner = false;
    // },
    //   err => {
    //     this.matSpinner = false;
    //     this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
    //       duration: 7000,
    //     });
    // });
  }

  asignarInformantes() {
    this.lst_asignar.forEach(item_to_add => {
      for (let index = 0; index < this.dataSource.data.length; index++) {
        if (item_to_add.Id_Informante === this.dataSource.data[index].Id_Informante) {
          this.lst_informantes_asignados.push(this.dataSource.data[index]);
          this.dataSource.data.splice(index, 1);
        }
      }
    });
    // this.applyFilter();
    this.CalcularProductos();
    this.snackBar.open('Informantes Asignados Correctamente', 'Cerrar', {
      duration: 3000,
    });
  }

  desasignarInformantes() {
    this.lst_desasignar.forEach(item_to_remove => {
      for (let index = 0; index < this.lst_informantes_asignados.length; index++) {
        if (item_to_remove.Id_Informante === this.lst_informantes_asignados[index].Id_Informante) {
          this.dataSource.data.push(this.lst_informantes_asignados[index]);
          this.lst_informantes_asignados.splice(index, 1);
        }
      }
    });
    // this.applyFilter();
    this.CalcularProductos();
    this.snackBar.open('Informantes Desasignados Correctamente', 'Cerrar', {
      duration: 3000,
    });
  }

  // filterByCUIT(filterValue: string) {
  //   this.filtro.Cuit = filterValue;
  //   this.applyFilter();
  // }

  // filterByBase(filterValue: string) {
  //   this.filtro.Id_Base = filterValue;
  //   this.applyFilter();
  // }

  // filterByTipoNegocio(filterValue: string) {
  //   this.filtro.Id_Tipo_Informante = filterValue;
  //   this.applyFilter();
  // }

  // filterByRubro(filterValue: string) {
  //   this.filtro.Rubro = filterValue;
  //   this.applyFilter();
  // }

  // filterByEstado(filterValue: string) {
  //   this.filtro.Id_Estado_Informante = filterValue;
  //   this.applyFilter();
  // }

  // filterById(filterValue: string) {
  //   this.filtro.Id_Informante = filterValue;
  //   this.applyFilter();
  // }

  // filterByRazon_social(filterValue: string) {
  //   this.filtro.Referencia = filterValue;
  //   this.applyFilter();
  // }

  // filterByDomicilio(filterValue: string) {
  //   this.filtro.Domicilio = filterValue;
  //   this.applyFilter();
  // }

  // filterByCaptacion(filterValue: string) {
  //   this.filtro.Id_Medio_Captacion = filterValue;
  //   this.applyFilter();
  // }

  // getFilterString(): string {
  //   let result = '';
  //   result += this.filtro.Id_Informante;
  //   result += ';' + this.filtro.Cuit;
  //   result += ';' + this.filtro.Referencia;
  //   result += ';' + this.filtro.Domicilio;
  //   result += ';' + this.filtro.Rubro;
  //   result += ';' + this.filtro.Id_Tipo_Informante;
  //   result += ';' + this.filtro.Id_Estado_Informante;
  //   result += ';' + this.filtro.Id_Base;
  //   result += ';' + this.filtro.Id_Medio_Captacion;
  //   return result;
  // }

  // applyFilter() {
  //   const filterValue = this.getFilterString().trim().toLowerCase();
  //   this.dataSource.filterPredicate = function (data, filter: string): boolean {
  //     return true
  //       && InformantesComponent.equalsFilter(filter, 0, data.Id_Informante)
  //       && InformantesComponent.includesFilter(filter, 1, data.Cuit)
  //       && InformantesComponent.includesFilter(filter, 2, data.Referencia)
  //       // && InformantesComponent.includesFilter(filter, 3, data.domicilio.toString())
  //       && InformantesComponent.existInArrayFilter(filter, 4, data.RubrosAsignados)
  //       && InformantesComponent.equalsFilter(filter, 5, data.Id_Tipo_Informante)
  //       && InformantesComponent.equalsFilter(filter, 6, data.Id_Estado_Informante)
  //       && InformantesComponent.equalsFilter(filter, 7, data.Id_Base)
  //       && InformantesComponent.equalsFilter(filter, 8, data.Id_Medio_Captacion);
  //   };

  //   this.dataSource.filter = filterValue;
  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  //   this.animarSeleccionado(null);
  // }

  animarSeleccionado(informante_seleccionado) {
    this.lst_informantes_asignados.forEach(element => {
      element.animacion = null;
    });

    this.dataSource.data.forEach(element => {
      element.animacion = null;
    });
    setTimeout(() => {
      if (informante_seleccionado) {
        informante_seleccionado.animacion = 'BOUNCE';
      }
    }, 500);
  }

  ubicarInformanteEnMapa(informante_seleccionado) {
    if (informante_seleccionado) {
      this.camera = { lat: Number(informante_seleccionado.Latitud), lng: Number(informante_seleccionado.Longitud) };
      this.ubicarInformanteEnLista(informante_seleccionado);
      this.openWindow(informante_seleccionado.Id_Informante);
    }
  }

  ubicarInformanteEnLista(informante_seleccionado) {
    this.camera = { lat: Number(informante_seleccionado.Latitud), lng: Number(informante_seleccionado.Longitud) };
    this.animarSeleccionado(informante_seleccionado);
    this.lst_informantes.options.forEach(i_option => {
      if (i_option.value.Id_Informante.toString() === informante_seleccionado.Id_Informante.toString()) {
        i_option.focus();
      }
    });
  }

  asignarMarkers() {
    this.dataSource.data.forEach(element => {
      if (element.Id_Estado === 1) {
        element.marker = '../../../assets/img/icons/Check.png';
      } else {
        element.marker = '../../../assets/img/icons/PSA.png';
      }
    });
  }

  confirmarHojaDeRuta() {
    const dialogConfirmDelete = this.dialog.open(ConfirmacionComponent, {
      width: '400px',
      data: { titulo: '¿Realmente desea generar esta hoja de ruta?' }
    });
    dialogConfirmDelete.afterClosed().subscribe(result => {
      if (result === 'SI') {
        this.generarHojaDeRuta();
      }
    });
  }

  generarHojaDeRuta() {
    let idsInformantes = '';
    this.lst_informantes_asignados.forEach(element => {
      idsInformantes += ',' + element.Id_Informante;
    });
    idsInformantes = idsInformantes.substring(1);

    this.nueva_hoja_ruta.idsInformantes = idsInformantes;

    this.matSpinner = true;
    this.hojaDeRutaService.insertarHojaRuta(this.nueva_hoja_ruta).subscribe(data => {
      // Si es -2 es porque la hoja de ruta ya existe
      if (data === -2) {
        this.dialog.open(InformacionComponent, {
          width: '400px',
          data: { titulo: 'Atención !!', texto: 'El nombre de la Hoja de Ruta ya existe' }
        });
        this.nueva_hoja_ruta.N_HojaDeRuta = '';
      // Si no guarda normal
      } else {
        this.snackBar.open('La hoja de ruta ha sido generada con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.route.navigateByUrl('\hoja-de-ruta');
      }
      this.matSpinner = false;
    },
      err => {
        console.log(err);
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
    });
  }

  cancelar() {
    const dialogConfirmDelete = this.dialog.open(ConfirmacionComponent, {
      width: '400px',
      data: { titulo: '¿Está seguro que desea cancelar?', texto: 'Si continúa se desestimará la hoja de ruta.' }
    });
    dialogConfirmDelete.afterClosed().subscribe(result => {
      if (result === 'SI') {
        this.route.navigateByUrl('\hoja-de-ruta');
      }
    });
  }

  CalcularProductos() {
    let idsInformantes = '';
    this.lst_informantes_asignados.forEach(element => {
      idsInformantes += ',' + element.Id_Informante;
    });
    idsInformantes = idsInformantes.substring(1);

    this.nueva_hoja_ruta.idsInformantes = idsInformantes;
    if (idsInformantes.length > 0) {
    //   this.matSpinner = true;
    //   this.informantesService.obtenerCantidadDeProductosByInformantes(idsInformantes).subscribe(data => {
    //     this.cantidad_productos_asignados = data;
    //     this.matSpinner = false;
    //   },
    //   err => {
    //     this.matSpinner = false;
    //     this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
    //       duration: 7000,
    //     });
    // });
    } else {
      this.cantidad_productos_asignados = 0;
    }

  }

  cargarNumeroHojaRutaNueva() {
    this.hojaDeRutaService.obtenerListadoHojaRuta().subscribe(data => {
        this.nueva_HojaDeRuta_Numero = data.length + 1;
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
    });
  }

  openWindow(id) {
      this.openedWindowInMarker = id;
  }

  isInfoWindowOpen(id) {
      return this.openedWindowInMarker === id;
  }

}
