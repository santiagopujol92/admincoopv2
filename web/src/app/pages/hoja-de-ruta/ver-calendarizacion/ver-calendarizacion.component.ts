import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { HojaDeRutaService } from 'src/app/services/hoja-de-ruta.service';

@Component({
  selector: 'app-ver-calendarizacion',
  templateUrl: './ver-calendarizacion.component.html',
  styleUrls: ['./ver-calendarizacion.component.css']
})
export class VerCalendarizacionComponent implements OnInit {
  encuestadores: any[] = [];
  encuestadores1: any[] = [];
  matSpinner = true;
  fechas1: any[] = [];
  periodo: any[];
  constructor(public dialogRef: MatDialogRef<VerCalendarizacionComponent>, public _serviceHojaDeRuta: HojaDeRutaService,
    public snackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any) {

    this.periodo = data.fecha;
  }

  ngOnInit() {
    this.cargarHojasDeRuta();
  }

  cerrar(): void {
    this.dialogRef.close();
  }

  cargarHojasDeRuta() {
     if (this.periodo.length === 0) {
      console.log('soy null');
      this.periodo = [];
      const mess = new Date().toLocaleDateString('sp-US', { month: 'long' });
      const fechaActual = new Date();
      const mesActual = fechaActual.toLocaleDateString('sp-AR', { month: 'numeric'});
      const anioActual = String(fechaActual.getFullYear());
      console.log(mesActual);
      console.log(anioActual);
      this.periodo.push(mesActual);
      this.periodo.push(anioActual);
    }

    console.log(this.periodo);
    const arrayFecha = this.periodo;
    const mes = arrayFecha[0];
    const anio = arrayFecha[1];
    this._serviceHojaDeRuta.obtenerListadoHRByMesesAnios(mes + '-' + anio).subscribe(data => {
      this.obtenerFechas(data);
      this.obtenerEncuestadores(data);
      this.matSpinner = false;
    },
      err => {
        this.matSpinner = false;
        this.snackBar.open('Disculpe.. sucedio un problema. Comunicarse con Daniel!', 'Cerrar', {
          duration: 7000,
        });
      });
  }

  // Método para llenar el nombre de las columnas dinamicamente
  // Obtenermos todas las fechas DINAMICAMENTE del listado
  obtenerFechas(listado) {
    listado.forEach(element => {
      if (element.fecha !== null) {
        this.fechas1.push(element.fecha);
      }
    });

    // Eliminamos las repetidas
    const eliminarRepetidos = (fechas1) => {
      const unicos = [];
      const itemsEncontrados = {};
      for (let i = 0, l = fechas1.length; i < l; i++) {
        const stringified = JSON.stringify(fechas1[i]);
        if (itemsEncontrados[stringified]) { continue; }
        unicos.push(fechas1[i]);
        itemsEncontrados[stringified] = true;
      }
      return unicos;
    };
    this.fechas1 = eliminarRepetidos(this.fechas1);

    // Ordenamos las fechas
    function sortFunction(a, b) {
      const dateA = new Date(a).getTime();
      const dateB = new Date(b).getTime();
      return dateA > dateB ? 1 : -1;
    }
    this.fechas1.sort(sortFunction);
    console.log(this.fechas1);

  }

  obtenerEncuestadores(listado) {
    listado.forEach(element => {
      if (element.N_Encuestador !== null) {
        /*  const enc = {
           N_Encuestador: element.N_Encuestador,
           fecha: element.fecha
         };
         this.encuestadores1.push(enc); */
        const arr1: any[] = [];
        const enc = {
          N_Encuestador: element.N_Encuestador,
          listHoja: arr1
        };
        enc.listHoja.push();
        this.encuestadores.push(enc);
      }
    });

    /*  console.log(this.encuestadores1); */

    const eliminarRepetidos = (encuestadores) => {
      const unicos = [];
      const itemsEncontrados = {};
      for (let i = 0, l = encuestadores.length; i < l; i++) {
        const stringified = JSON.stringify(encuestadores[i]);
        if (itemsEncontrados[stringified]) { continue; }
        unicos.push(encuestadores[i]);
        itemsEncontrados[stringified] = true;
      }
      return unicos;
    };
    this.encuestadores = eliminarRepetidos(this.encuestadores);
    /*   console.log(this.encuestadores);
      console.log(listado); */

    this.obtenerEncuestadoresFecha(this.encuestadores, listado);
  }

  obtenerEncuestadoresFecha(encuestadores, listado) {
    this.encuestadores.forEach(listEnc => {
      this.fechas1.forEach(listFechas => {
        const columns = {
          fecha: listFechas,
          N_HojaDeRuta: null
        };
        listEnc.listHoja.push(columns);
      });
    });
    console.log(listado);

    listado.forEach(listHojaRuta => {
      encuestadores.forEach(listEnc => {
        listEnc.listHoja.forEach(element => {
          if (element.fecha === listHojaRuta.fecha && listEnc.N_Encuestador === listHojaRuta.N_Encuestador) {
            element.N_HojaDeRuta = String(element.N_HojaDeRuta).concat(', ' + listHojaRuta.N_HojaDeRuta);
            element.N_HojaDeRuta = (element.N_HojaDeRuta).replace('null,', '');
          }
        });
      });
    });
  }
}

