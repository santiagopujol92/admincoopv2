$(document).ready(function(){
	startDatatable('.datatable');
	activarMenu('facturas');
	startDatePicker('.datepicker');
	//Configuracion de modal edit y add
	configEditAddModal('modal-center2 modal-lg');
})

var token = $('meta[name="csrf-token"]').attr('content');

//Inicializar formulario como esta por default donde corresponda
function restartConfigForm(){
	$("#form"+form+" select[name=id_product]").attr('disabled', true).selectpicker('refresh');
	$("#form"+form+" input[name=payment_date]").attr('disabled', true);
	enableFieldsByPaymentMethod('form'+form, 0, 'div_tarjet_method_payment');
}

/* DATOS DINAMICOS A COMPLETAR PARA CADA ABM */
var modulo_msg = 'Factura';
var form = 'Factura';
var module = 'facturas';
var modals_btns = 'Invoice';

function listar(){
	var route = current_route+"_listar";

	var tabla_datos = $("#tbody_"+module+"");
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos.empty();
		$(result).each(function(key,value){

			var cliente = '';
			if (value.id_person == 1 || value.id_person == null){
				cliente = value.business;
			}else{
				cliente = value.lastname+ ' ' + value.name;
			}

			tabla_datos.append('<tr>'
					+'<td>'+value.invoice_number+'</td>'
					+'<td>'+cliente+'</td>'
					+'<td>'+value.product+'</td>'
					+'<td>$ '+value.total_amount+'</td>'
					+'<td>'+formatDateTime(value.created_at, 'd-m-Y h:m:s')+'</td>'
					+'<td>'
						+'<div class="icon-button-demo">'
							+'<button type="button" href="#" data-toggle="modal" title="Editar" onclick="mostrarEnModal('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
                            	+'<i class="material-icons">edit</i>'
                           	+'</button>'
        	             	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
                				+'<i class="material-icons">delete</i>'
                    		+'</button>'
						+'</div>'
					+'</td>'
				+"</tr>");
		});
	});
	statusLoading('loading_list', 0);
	startDatatable('.datatable');
 }

/*ABRIR MODAL EDIT CON DATOS*/
function mostrarEnModal(id){
	$("#modal_mensaje").addClass('hidden');
	var route = current_route+"/"+id+"/edit";

	statusLoading('loading_modal', 1);

	$.get(route, function(result){

		$("#form"+form+" input[name=invoice_number]").val(result[0]['invoice_number']);
		$("#form"+form+" input[name=total_amount]").val(result[0]['total_amount']);

		/*INPUTS DE TIPO DE PAGO TARJETA*/
		$("#form"+form+" input[name=fees]").val(result[0]['fees']);
		$("#form"+form+" input[name=parcial_amount]").val(result[0]['parcial_amount']);
		$("#form"+form+" input[name=administrative_expenses]").val(result[0]['administrative_expenses']);
		/**/
		
		$("#form"+form+" input[name=issue_date]").val(formatDateTime(result[0]['issue_date'], 'd-m-Y'));
		$("#form"+form+" input[name=effective_date]").val(formatDateTime(result[0]['effective_date'], 'd-m-Y'));
		
		//ASIGNAR DE ACUERDO AL id_person o id_legal_person EL QUE SEA MAYOR A 1 SE UTILIZA ESE ID
		//Y SETEO DEL RADIO
		if (result[0]['id_legal_person'] > 1 && (result[0]['id_person'] <= 1 || result[0]['id_person'] == '')){
			var id_type_person = result[0]['id_type_people_business'];
			$("#form"+form+" [name=type_person][value='e']").prop('checked', true);
			$("#form"+form+" select[name=id_legal_person]").removeAttr('disabled');
			//ACTIVAMOS EL SELECT DE CLIENTE DE ACUERDO AL VALOR SI ENTRO ACA
			showSelectPerson("form"+form, 'e');
		}
		if (result[0]['id_person'] > 1 && (result[0]['id_legal_person'] <= 1 || result[0]['id_legal_person'] == '')){
			var id_type_person = result[0]['id_type_people_person'];
			$("#form"+form+" [name=type_person][value='p']").prop('checked', true);
			$("#form"+form+" select[name=id_person]").removeAttr('disabled');
			//ACTIVAMOS EL SELECT DE CLIENTE DE ACUERDO AL VALOR SI ENTRO ACA
			showSelectPerson("form"+form, 'p');
		}

		$("#form"+form+" select[name=id_type_person]").val(id_type_person).selectpicker('refresh');
		$("#form"+form+" select[name=id_legal_person]").val(result[0]['id_legal_person']).selectpicker('refresh');
		$("#form"+form+" select[name=id_person]").val(result[0]['id_person']).selectpicker('refresh');
		$("#form"+form+" select[name=id_status_invoice]").val(result[0]['id_status_invoice']).selectpicker('refresh');
		$("#form"+form+" select[name=id_type_product]").val(result[0]['id_type_product']).selectpicker('refresh');

		$("#form"+form+" select[name=id_product]").removeAttr('disabled');
		$("#form"+form+" select[name=id_product]").val(result[0]['id_product']).selectpicker('refresh');

		$("#form"+form+" select[name=id_person]").val(result[0]['id_person']).selectpicker('refresh');
		$("#form"+form+" select[name=id_payment_method]").val(result[0]['id_payment_method']).selectpicker('refresh');
		
		//LLAMADA A LA FUCNION QUE HABILITA LOS DIVS DETIPOS DE PAGO CON EL VALOR DE ESTA FACTURA
		enableFieldsByPaymentMethod("form"+form, result[0]['id_payment_method'], 'div_tarjet_method_payment');

		if (result[0]['paid'] == '1'){
			paid_check = true;
		}else{
			paid_check = false;
		}

		$("#form"+form+" input[type=checkbox][name=paid]").prop('checked', paid_check);
		//ACTIVAMOS EL CAMPO DE FECHA DE PAGO DE ACUERDO AL VALOR DE PAGADO.
		enableInputAfterCheck('paid', 'form'+form, 'payment_date');
		$("#form"+form+" input[name=payment_date]").val(formatDateTime(result[0]['payment_date'], 'd-m-Y'));

		//SI EL TIPO DE PAGO ES TARJETA HABILITAR EL HIDDEN DEL DIV DE TARJETA
		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+')');
	});
	statusLoading('loading_modal', 0);
}

// Mostrar Select de personas a partir de unvalor de radio y configurarlo
function showSelectPerson(formId, value){
	if (value == 'e'){
		$("#"+formId+" #div_select_person").addClass('hidden');
		$("#"+formId+" #div_select_legal_person").removeClass('hidden');
		//Se le asigna el valor 1 porq es el por defecto Registro 1 "Persona No Asignado"
		$("#"+formId+" #div_select_person select[name=id_person]").val(1).selectpicker('refresh');

		//Se resetea el select de tipo de persona y Se modifica la funcion on click del select de type person de acuerdo a la seleccion
		$("#"+formId+" select[name=id_type_person]").val(0).selectpicker('refresh');
		$("#"+formId+" select[name=id_type_person]").attr('onchange', "changeDataSelectTarget('empresas', 'findByTypePersonId', 'id_legal_person', 'Empresa', this.form.id, this.value, 0, false);");
		return false;
	}
	if (value == 'p'){
		$("#"+formId+" #div_select_legal_person").addClass('hidden');
		$("#"+formId+" #div_select_person").removeClass('hidden');
		//Se le asigna el valor 1 porq es el por defecto Registro 1 "Empresa No Asignado"
		$("#"+formId+" #div_select_legal_person select[name=id_legal_person]").val(1).selectpicker('refresh');

		//Se resetea el select de tipo de persona y Se modifica la funcion on click del select de type person de acuerdo a la seleccion
		$("#"+formId+" select[name=id_type_person]").val(0).selectpicker('refresh');
		$("#"+formId+" select[name=id_type_person]").attr('onchange', "changeDataSelectTarget('personas', 'findByTypePersonId', 'id_person', 'Persona', this.form.id, this.value, 0, false);");
		return false;
	}
}

//Desabilitar y habilitar un input desp de un check
function enableInputAfterCheck(checkName, formId, inputTargetName){
	var element = $("#"+formId+" input[type=checkbox][name="+checkName+"]");

	if (element.prop('checked') == true){
		$("#"+formId+" input[name="+inputTargetName+"]").prop('disabled', false);
		$("#"+formId+" input[name="+inputTargetName+"]").prop('required', true);
		$("#"+formId+" #span_"+inputTargetName).html('<label class="col-red">*</label> <i class="material-icons">date_range</i>');
	}else{
		$("#"+formId+" input[name="+inputTargetName+"]").val('');
		$("#"+formId+" input[name="+inputTargetName+"]").prop('disabled', true);
		$("#"+formId+" input[name="+inputTargetName+"]").prop('required', false);
		$("#"+formId+" #span_"+inputTargetName).html('<i class="material-icons">person</i>');
	}
}

function enableFieldsByPaymentMethod(formId, value, divTargetId){
	if (value == 1){
		$("#"+formId+" #"+divTargetId+"").removeClass('hidden');
	}else{
		$("#"+formId+" #"+divTargetId+"").addClass('hidden');
		$("#"+formId+" #"+divTargetId+" input").val('');
	}
}