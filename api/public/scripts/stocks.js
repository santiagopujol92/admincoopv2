$(document).ready(function(){
	startDatatableExportable('.datatable');
	activarMenu('stocks');
});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPELTAR PARA CADA ABM */
var modulo_msg = 'Stock';
var form = 'Stock';
var module = 'stocks';
var modals_btns = 'Stock';

function listar(){
	var route = current_route+"_listar";
	
	var tabla_datos = $("#tbody_"+module+"");
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos.empty();
		$(result).each(function(key,value){

			//Validacion y Formateo de fecha
			var last_change_stock;
			if(value.last_change_stock == '0000-00-00 00:00:00' || value.last_change_stock == null){
				last_change_stock = '-';
			}else{
				last_change_stock = formatDateTime(value.last_change_stock, 'd-m-Y h:i:s');
			}

			//Discriminacion de valores para mejor visual
			var bg_stock = ' bg-green ';
			var status_stock = '(Alto Stock)';
			//Si el stock esta en 0
			if (value.stock == '0'){
				bg_stock = ' bg-red '
				status_stock = '(Sin Stock)';
			}else{
				//Para Gramos y unidades
				if (value.type == 39 && value.stock < 1000 || value.type == 40 && value.stocko < 10){
					bg_stock = ' bg-orange ';
					status_stock = '(Bajo Stock)';
				}
			}

			var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"', false, false, true);";

			tabla_datos.append('<tr>'
					+'<td onclick="'+funcion_opciones+'">'+value.description+'</td>'
					+'<td onclick="'+funcion_opciones+'" class="col-teal '+bg_stock+' "><b>'+value.stock+' '+value.type_description+'</b> '+status_stock+'</td>'
					+'<td onclick="'+funcion_opciones+'">'+last_change_stock+'</td>'
					+'<td>'
						+'<div class="icon-button-demo">'
							+'<button type="button" href="#" data-toggle="modal" title="Aumentar Stock" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-green btn-circle waves-effect waves-circle waves-float">'
                            	+'<i class="material-icons">add</i>'
                           	+'</button>'
						+'</div>'
					+'</td>'
				+"</tr>");
		});
	});
	statusLoading('loading_list', 0);
	startDatatableExportable('.datatable');
 }

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){
	clearForm("form"+form);	

	$.get( route, function(result) {
		//Titulo Modificado
		$("#modal"+modals_btns+" .modal-title").html('STOCK DE: <b class="col-teal">'+result[0]['description']+'</b>');

		$("#form"+form+" input[name=stock]").val(result[0]['stock']);
		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');
		$("#form"+form+" select[name=type]").val(result[0]['type']).selectpicker('refresh');
		$("#form"+form+" #label_unidad, #form"+form+" #label_unidad2").html('(' + result[0]['type_description'] + ')');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
	});
}