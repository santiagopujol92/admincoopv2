$(document).ready(function(){
	startDatatableExportable('.datatable');
	activarMenu('admin', 'productos_venta');
	configEditAddModal('modal-center4 modal-lg');
});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPELTAR PARA CADA ABM */
var modulo_msg = 'Producto Venta';
var form = 'ProductoVenta';
var module = 'productos_venta';
var modals_btns = 'SaleProduct';

function listar(){
	var route = current_route+"_listar";
	
	var tabla_datos = $("#tbody_"+module+"");
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos.empty();
		$(result).each(function(key,value){

			var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"');";

			tabla_datos.append('<tr>'
					+'<td onclick="'+funcion_opciones+'">'+value.description+'</td>'
					+'<td onclick="'+funcion_opciones+'">'+value.sale_type_description+'</td>'
					+'<td>'
						+'<div class="icon-button-demo">'
							+'<button type="button" href="#" data-toggle="modal" title="Editar" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
                            	+'<i class="material-icons">edit</i>'
                           	+'</button>'
        	             	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
                				+'<i class="material-icons">delete</i>'
                    		+'</button>'
						+'</div>'
					+'</td>'
				+"</tr>");
		});
	});
	statusLoading('loading_list', 0);
	startDatatableExportable('.datatable');
 }

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){

	$.get(route, function(result){
		$("#form"+form+" input[name=description]").val(result.description);
		$("#form"+form+" select[name=id_sale_type_product]").val(result.id_sale_type_product).selectpicker('refresh');
		$("#form"+form+" select[name=type_metric_product]").val(result.type_metric_product).selectpicker('refresh');
		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
	});
}