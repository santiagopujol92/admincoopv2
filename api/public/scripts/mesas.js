$(document).ready(function(){
	startDateTimePicker('.datetimepicker');
	activarMenu('mesas');
});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPELTAR PARA CADA ABM */
var modulo_msg = 'Mesa';
var form = 'Mesa';
var module = 'mesas';
var modals_btns = 'Table';

function listar(){
	var route = current_route+"_listar";
	// var route = url.origin+"/ventas_listar";

	var tabla_datos = $("#tbody_"+module+"");
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos.empty();
		$(result).each(function(key,value){

			var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"', true, true, false, true, true);";
			var color = '';
			if (value.id_status_table == 1){
				color = 'bg-red';
			}else if (value.id_status_table == 2){
				color = 'bg-green';
			}else if (value.id_status_table == 3){
				color = 'bg-orange';
			}

			tabla_datos.append('<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">'
                +'<button title="Mesa '+value.status+'" onclick="'+funcion_opciones+'" class="btn btn-link waves-effect btn-block '+color+'" type="button">'
                    +'<div class="demo-google-material-icon"> '
                    	+'<span class="icon-name">Mesa '+value.description+'</span> '
                   	+'</div>'
        		+'</button>'
				+"</div>");
		});
	});
	statusLoading('loading_list', 0);
 }

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){
	$.get(route, function(result){
		changeSelectSaleByValue("form"+form, result.id_status_table, 'id_sale');
		configButtonEditSale("form"+form, result.id_sale, 'btn_sale_table');

		$("#form"+form+" input[name=description]").val(result.description);
		$("#form"+form+" select[name=id_status_table]").val(result.id_status_table).selectpicker('refresh');
		$("#form"+form+" select[name=id_sale]").val(result.id_sale).selectpicker('refresh');
		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');
		$("#form"+form+" button[name=btn_sale_table]").attr('onclick', 'prepareModalDataEditVenta('+result.id_sale+')');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
	});
}

//Cambiar estado a libre y desvincular venta de la mesa
function liberarMesa(id){
    var route = current_route+"/"+id;
    var datos = "&id_status_table=2&id_sale=0";

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	activeLoadingInButton('btn_modal_opc_unlock_Table' , 1, 'LIBERANDO MESA ..', 'col-white');
        },
		success: function(result){
			$("#modal_opciones").modal('hide');
			listar();
			activeLoadingInButton('btn_modal_opc_unlock_Table' , 0, '<i class="material-icons">lock_open</i><b> LIBERAR MESA</b>', 'col-white');
			showMessageModal(modulo_msg+' Liberada con Éxito', 'Buen Trabajo!', 'bg-green', 'done');
		},
		error: function(){
			$("#modal_opciones").modal('hide');
			showMessageModal('No se pudo Liberar la '+modulo_msg+'. <br>Intente Nuevamente', 'ATENCIÓN', 'bg-yellow', 'warning');
			activeLoadingInButton('btn_modal_opc_unlock_Table' , 0, '<i class="material-icons">lock_open</i><b> LIBERAR MESA</b>', 'col-white');
		}
	})	
}

//abrir modal para seleccionar una venta y al aceptar Cambiar estado a ocupado
function prepararModalOcuparMesa(id_reg){
	//Traer la venta para mostrarla en el select
	var route = root_project+module+"/"+id_reg+"/edit";

	var titulo = 'OCUPAR MESA';
	$(".modal-title").html(titulo.toUpperCase());

	$.get(route, function(result){
		configButtonEditSale("formOcuparMesa", result.id_sale, 'btn_sale_table');
		$("#formOcuparMesa select[name=id_sale]").val(result.id_sale).selectpicker('refresh');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("formOcuparMesa");
	});

	$("#modal_opciones").hide();
	$("modalOcuparMesa").show();
	$("#formOcuparMesa button[name=btnguardarOcuparMesa]").attr('onclick', 'ocuparMesa('+id_reg+');');
}

//Cambiar estado luego de haber seleccionado la venta y enviar tambien la venta
function ocuparMesa(id){
    var route = root_project+"/"+module+"/"+id;
	var id_sale = $("#formOcuparMesa select[name=id_sale]").val();
    var datos = "&id_status_table=1&id_sale="+id_sale;

	var array_validate = checkFormShowingMsg("formOcuparMesa");
	var msg = '';
	if (array_validate['puedeGuardar'] == false){
		msg = array_validate['msg'];
		showMessage('modal_mensaje_ocupar_mesa', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
		return false;
	}

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	activeLoadingInButton('formOcuparMesa button[name=btnguardarOcuparMesa]' , 1, 'ASIGNANDO VENTA A MESA ..', 'col-black');
        },
		success: function(result){
			$("#modal_opciones").modal('hide');
			$("#modalOcuparMesa").modal('hide');
			listar();
			activeLoadingInButton('formOcuparMesa button[name=btnguardarOcuparMesa]' , 0, 'GUARDAR VENTA EN MESA');		
			showMessageModal(modulo_msg+' Ocupada con Éxito', 'Buen Trabajo!', 'bg-green', 'done');
			createOrderByTable(id, id_sale);
			$(".modal-backdrop").removeClass('in');
		},
		error: function(){
			$("#modal_opciones").modal('hide');
			$("#modalOcuparMesa").modal('hide');
			showMessageModal('No se pudo Ocupar la '+modulo_msg+'. <br>Intente Nuevamente', 'ATENCIÓN', 'bg-yellow', 'warning');
			activeLoadingInButton('formOcuparMesa button[name=btnguardarOcuparMesa]' , 0, 'GUARDAR VENTA EN MESA');
		}
	})	
}

//Crear orden por mesa al registrar una venta en una mesa para luego accederla desde bar y cocina
function createOrderByTable(id_table, id_sale){
    var route = root_project+"/"+module+"_createOrderByTable";
    var datos = "?id_table="+id_table+"&id_sale="+id_sale+"&id_status_order_by_table=1";

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'GET',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	// activeLoadingInButton('btn_modal_opc_reserve_Table' , 1, 'RESERVANDO MESA ..', 'col-white');
        },
		success: function(result){
			console.log(result);
			alert('se creo la orden de esta mesa para bar y cocina');
			// $("#modal_opciones").modal('hide');
			// listar();
			// activeLoadingInButton('btn_modal_opc_reserve_Table' , 0, '<i class="material-icons">phone_forwarded</i><b> RESERVAR MESA</b>', 'col-white');
			// showMessageModal(modulo_msg+' Reservada con Éxito', 'Buen Trabajo!', 'bg-green', 'done');
		},
		error: function(){
			// $("#modal_opciones").modal('hide');
			// showMessageModal('No se pudo Reservar la '+modulo_msg+'. <br>Intente Nuevamente', 'ATENCIÓN', 'bg-yellow', 'warning');
			// activeLoadingInButton('btn_modal_opc_reserve_Table' , 0, '<i class="material-icons">phone_forwarded</i><b> RESERVAR MESA</b>', 'col-white');
		}
	})	
}

//Cambiar el estado de la orden de la mesa(a preparar, a retirar, confirmada, cancelada) al cambiar el estado de la mesa(libre, reservada)
function changeStatusOrderByTable(id_table, id_status_order){

}

//Cambiar estado a reservada y desvincular venta de la mesa
function reservarMesa(id){
    var route = root_project+"/"+module+"/"+id;
    var datos = "&id_status_table=3&id_sale=0";

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	activeLoadingInButton('btn_modal_opc_reserve_Table' , 1, 'RESERVANDO MESA ..', 'col-white');
        },
		success: function(result){
			$("#modal_opciones").modal('hide');
			listar();
			activeLoadingInButton('btn_modal_opc_reserve_Table' , 0, '<i class="material-icons">phone_forwarded</i><b> RESERVAR MESA</b>', 'col-white');
			showMessageModal(modulo_msg+' Reservada con Éxito', 'Buen Trabajo!', 'bg-green', 'done');
		},
		error: function(){
			$("#modal_opciones").modal('hide');
			showMessageModal('No se pudo Reservar la '+modulo_msg+'. <br>Intente Nuevamente', 'ATENCIÓN', 'bg-yellow', 'warning');
			activeLoadingInButton('btn_modal_opc_reserve_Table' , 0, '<i class="material-icons">phone_forwarded</i><b> RESERVAR MESA</b>', 'col-white');
		}
	})	
}

//Interaccion de select de id_sale al cambiar id_status_table
function changeSelectSaleByValue(form_id, value_selected, target_name_select){
	//Ocupada
	if(value_selected == 1){
		$("#"+form_id+" select[name="+target_name_select+"]").prop('required', true);
		$("#"+form_id+" select[name="+target_name_select+"]").val(0).selectpicker('refresh');
		$("#"+form_id+" #span_id_sale_by_tables").html('<label class="col-red">*</label> <i class="material-icons">assignment</i>');
		$("#div_select_id_sale_tables").removeClass('hidden');
	//Libre
	}else if(value_selected == 2){
		$("#"+form_id+" select[name="+target_name_select+"]").prop('required', false);
		$("#"+form_id+" select[name="+target_name_select+"]").val(0).selectpicker('refresh');
		$("#"+form_id+" #span_id_sale_by_tables").html('<i class="material-icons">assignment</i>');
		$("#div_select_id_sale_tables").addClass('hidden');
	//Reservada
	}else if (value_selected == 3){
		$("#"+form_id+" select[name="+target_name_select+"]").prop('required', false);
		$("#"+form_id+" select[name="+target_name_select+"]").val(0).selectpicker('refresh');
		$("#"+form_id+" #span_id_sale_by_tables").html('<i class="material-icons">assignment</i>');
		$("#div_select_id_sale_tables").addClass('hidden');
	//Por Defecto
	}else{
		$("#"+form_id+" select[name="+target_name_select+"]").prop('required', false);
		$("#"+form_id+" select[name="+target_name_select+"]").val(0).selectpicker('refresh');
		$("#"+form_id+" #span_id_sale_by_tables").html('<i class="material-icons">assignment</i>');
		$("#div_select_id_sale_tables").addClass('hidden');
	}
}

function restartConfigDefaultFormMesa(){
	changeSelectSaleByValue("form"+form, 0, 'id_sale');
	configButtonEditSale("form"+form, 0, 'btn_sale_table');
}



/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

// REPITIENDO FUNCIONES DE ABM PARA VENTAS

/* AGREGAR */
function agregarVenta(validar, form){
	var modulo_msg = 'Venta';
	var module = 'ventas';
	var modals_btns = 'Sale';

	$("#modal_mensaje").addClass('hidden');

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		var array_validate = checkFormShowingMsg("form"+form);
		var msg = '';
		if (array_validate['puedeGuardar'] == false){
			msg = array_validate['msg'];
			showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
			return false;
		}

		//Validar email si existe el input con name email
		if ($("#form"+form+" input[name=email]").length > 0){
			if ($("#form"+form+" input[name=email]").val() != ''){
				if (!isEmail($("#form"+form+" input[name=email]").val())){
					showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>El email ingresado no es valido', 'error');
					return false;
				}
			}
		}
		
	}

	var ruta = current_route;
	//Reemplazo porq necesito acceder a ventas y aca toma la ruta donde estoy
	var route = ruta.replace('mesas', 'ventas');

	var datos = $("#form"+form+"").serialize();
		$.ajax({
			url: route,
			headers: {  'X-CSRF-TOKEN': token},
			type: 'POST',
			dataType: 'json',
			data:datos,
            beforeSend: function () {
             	statusLoading('loading_modal', 1, 'Agregando '+modulo_msg+' ..');
            },
			success: function(result){
				$("#smallModalMessage").css('z-index', 1101);
				//Si la respuesta_custom existe
				if (result.response_custom){

					//Si la respuesta custom es stock_error
					if (result.response_custom == 'stock_error'){
						//Recorremos los productos sin stock
						var productos = '';
						for (var i = 0; i < result.productos_sin_stock.length; i++) {
						   	var producto = result.productos_sin_stock[i];
							productos = productos + '- ' + producto + '<br>';
						}		
						showMessageModal('No se pudo cargar la '+modulo_msg+'.<br><br>Productos que no tienen disponible stock para las comidas seleccionadas:<br><b class="col-red">' + productos + '</b>', 'Alerta de Stock!', 'bg-yellow', 'warning');
					}
					//Otras respuestas
					//code
					
				//Si no tiene respuesta custom muestra la generica
				}else{

					showMessageModal(''+modulo_msg+' Agregado/a con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');
					clearForm('form'+form+'');
					$('#modal'+modals_btns+'').modal('hide');

					//Actualizar select de ventas para q aparezca la nueva
					actualizarSelectByDB('ventas', 'getAllSales', 'id_sale', 'Venta', 'formOcuparMesa', 0);
					actualizarSelectByDB('ventas', 'getAllSales', 'id_sale', 'Venta', 'formMesa', 0);
				}
				statusLoading('loading_modal', 0);
			},
			error: function(result){
				$("#smallModalMessage").css('z-index', 1101);
				showMessageModal('No se pudo Agregar un/a '+modulo_msg+'. <br>Complete los campos obligatorios(*) y correctamente<br> En el peor de los casos puede que este ingresando algun dato que no pueda duplicarse.', 'ATENCION', 'bg-yellow', 'warning');
				statusLoading('loading_modal', 0);
			}
		})	
}

/* UPDATE */
function updateVenta(id, validar){
	var ruta = current_route+"/"+id+"";
	var route = ruta.replace('mesas', 'ventas');
	var datos = $("#formVenta").serialize();

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		var array_validate = checkFormShowingMsg("formVenta");
		var msg = '';
		if (array_validate['puedeGuardar'] == false){
			msg = array_validate['msg'];
			showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
			return false;
		}

		//Validar email si existe el input con name email
		if ($("#formVenta input[name=email]").length > 0){
			if ($("#formVenta input[name=email]").val() != ''){
				if (!isEmail($("#formVenta input[name=email]").val())){
					showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>El email ingresado no es valido', 'error');
					return false;
				}
			}
		}
	}

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	statusLoading('loading_modal', 1, 'Guardando ..');
        },
		success: function(result){
			$("#smallModalMessage").css('z-index', 1101);
			//Si la respuesta_custom existe
			if (result.response_custom){

				//Si la respuesta custom es stock_error
				if (result.response_custom == 'stock_error'){
					//Recorremos los productos sin stock
					var productos = '';
					for (var i = 0; i < result.productos_sin_stock.length; i++) {
					   	var producto = result.productos_sin_stock[i];
						productos = productos + '- ' + producto + '<br>';
					}		
					showMessageModal('No se pudo cargar la Venta.<br><br>Productos que no tienen disponible stock para las comidas seleccionadas:<br><b class="col-red">' + productos + '</b>', 'Alerta de Stock!', 'bg-yellow', 'warning');
				}
				//Otras respuestas
				//code
				
			//Si no tiene respuesta custom muestra la generica
			}else{
				showMessageModal('Venta Modificada con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');
				// listar();
				$('#modalSale').modal('hide');

				//Actualizar select de ventas para q aparezca la nueva
				actualizarSelectByDB('ventas', 'getAllSales', 'id_sale', 'Venta', 'formOcuparMesa', 0);
				actualizarSelectByDB('ventas', 'getAllSales', 'id_sale', 'Venta', 'formMesa', 0);
			}
			statusLoading('loading_modal', 0);
		},
		error: function(){
			$("#smallModalMessage").css('z-index', 1101);
			showMessageModal('No se pudo Modificar la Venta. <br>Asegurese de completar todos los datos obligatorios(*) y correctos', 'ATENCIÓN', 'bg-yellow', 'warning');
			statusLoading('loading_modal', 0);
		}
	})	
}

/*AL PRESIONAR ENTER ESTANDO EN EL MODAL DE EDIT EJECUTAR ACTION DEL BOTON*/
$("#modal"+modals_btns+"").keyup(function(event){
    if(event.keyCode == 13){
        $("#form"+form+" button[name=btnguardar"+modals_btns+"]").click();
    }
});


//Agregar clase al modal de edit y Add para el tamaño
function configEditAddModalVenta(class_modal){
	$("#modal_size_class").addClass(class_modal);
}

//Setear boton para agregar
function configSaveButtonAddVenta(){
	$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'agregar(true)');

	var titulo = 'Agregar Venta';
	$(".modal-title").html(titulo.toUpperCase());
}

/*ABRIR MODAL EDIT Y EJECUTAR FUNCION PARA TRAER DATOS*/
function prepareModalDataEditVenta(id){
	//Cerramos modal de opciones
	$("#modal_opciones").modal('hide');
	$("#modalSale").css('z-index', 1100);

	//Titulo modal

	var titulo = 'Editar Venta';
	$("#modalSale .modal-title").html(titulo.toUpperCase());

	$("#modal_mensaje").addClass('hidden');
	var ruta = current_route+"/"+id+"/edit";
	//Reemplazo porq necesito acceder a ventas y aca toma la ruta donde estoy
	var route = ruta.replace('mesas', 'ventas');

	$("#form"+form+"").attr('method', 'PUT');

	statusLoading('loading_modal', 1);
	getAndShowDataEditVenta(id, route);
}


/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEditVenta(id, route){

	resetValueFood();

	$.get(route, function(result){

		//Guardo los datos de venta en variable venta
		var venta = result.data_controller[0];

		//TITULO VA CON NRO DE VENTA
		var titulo = 'Editar '+modulo_msg+ ' <b style="color:brown;"> ' + venta.id + '</b>';
		$("#modalSale .modal-title").html(titulo.toUpperCase());

		//Guardo el array doble en variable comidas_por_venta para luego recorrerla
		var comidas_por_venta = result.data_foods_by_sale;

		//Recorro el array de datos de comidas de la venta
		for (var i = 0; i < comidas_por_venta.length; i++) {
		   	var comida = comidas_por_venta[i];
	
			//Seteo el checkbox correspondiente de comida
		   	$("#formVenta [type=checkbox]#check_food-"+comida.id_food).prop('checked', true);

		   	//Seteo el quantity q tiene cargado el comida
		   	$("#formVenta [name=quantity_food-"+comida.id_food+"]").val(comida.quantity);

		   	//Ejecuto la funcion de calculo de costo individual + acumulacion del global cada vez que seteo un input
		   	calcAndShowFoodByQuantity("formVenta", comida.quantity, 'check_food-'+comida.id_food, 'cost_price_food-'+comida.id_food, 'sell_price_food-'+comida.id_food);
		}		

		$("#formVenta select[name=id_person]").val(venta.id_person).selectpicker('refresh');
		$("#formVenta select[name=id_person_employee]").val(venta.id_person_employee).selectpicker('refresh');
		$("#formVenta input[name=cost_price]").val(venta.cost_price);
		$("#formVenta input[name=sell_price]").val(venta.sell_price);
		$("#formVenta input[name=sell_date]").val(formatDateTime(venta.sell_date, 'd-m-Y h:i:s'));
		$("#formVenta button[name=btnguardarSale]").attr('onclick', 'updateVenta('+id+', true)');

		//Setear funciones al boton de desbloquear productos
		$("#formVenta #btn_unlock_foods").attr('onclick', 'unlockFieldsSaleOnEdit();deleteFoodsOfSale('+id+')');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("formVenta");

		//Bloquear todo los inputs y botones de productos porque no puede ser modificado una vez hecha la venta
		lockFieldsSaleOnEdit();
		
		statusLoading('loading_modal', 0);
	});
}


//Configurar boton editar venta al cambiar el select de venta
function configButtonEditSale(form_id, value_selected, select_name){
	if (value_selected != 0){
		$("#"+form_id+" button[name="+select_name+"]").removeAttr('disabled');
		$("#"+form_id+" button[name="+select_name+"]").attr('onclick', 'prepareModalDataEditVenta('+value_selected+')');		
	}else{
		$("#"+form_id+" button[name="+select_name+"]").prop('disabled', true);
		$("#"+form_id+" button[name="+select_name+"]").attr('onclick', '');
	}
}

function configNewSaleByOcuparVenta(){
	$("#modalSale").css('z-index', 1100);
}