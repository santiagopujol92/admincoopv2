//Funcion para setear la cantidad de comida en 1 o 0 de acuerdo al check
function setQuantityFood(formId, inputCheckSourceId, idFood){
	sourceInput = $("#"+formId+ " #" +inputCheckSourceId);
	targetInput = $("#"+formId+" input[name=quantity_food-"+idFood+"]");

	if(sourceInput.prop('checked') == true){
		targetInput.val(1);
	}else{
		targetInput.val(0);
	}

	var quantityFood = $("#"+formId+" input[name=quantity_food-"+idFood+"]").val();
	calcAndShowFoodByQuantity(formId, quantityFood, 'check_food-'+idFood, 'cost_price_food-'+idFood, 'sell_price_food-'+idFood);
}

//Funcion para sumar o restar cantidad de acuerdo al boton + o -
function addRemoveQuantitySale(formId, idFood, value){
	targetInput = $("#"+formId+" input[name=quantity_food-"+idFood+"]");

	if (targetInput.val() >= 1 ){
		//Si el valor del input es 1 y el value -1 no deja restar mas
		if (targetInput.val() == 1 && value == -1){
			return false;
		}
		//Calculo
		targetInput.val(parseInt(targetInput.val()) + parseInt(value));

		var quantityFood = $("#"+formId+" input[name=quantity_food-"+idFood+"]").val();
		calcAndShowFoodByQuantity(formId, quantityFood, 'check_food-'+idFood, 'cost_price_food-'+idFood, 'sell_price_food-'+idFood);
	}
}

//Calculo de gasto con tipo de medicion y valor dinamico
function calcAndShowFoodByQuantity(formId, valueQuantity, checkInputName, labelCostFoodId, labelSellFoodId){
	var checkInputFood = $("#"+formId+" input[name="+checkInputName+"]");
	var labelCostFood = $("#"+formId+" #"+labelCostFoodId);
	var labelSellFood = $("#"+formId+" #"+labelSellFoodId);

	var costPriceFood = checkInputFood.attr('cost-price-food');
	var sellPriceFood = checkInputFood.attr('sell-price-food');

	//Calcular el costo de comida de acuerdo a la cantidad comida comprada con la cantidad ingresada
	var costFoodByQuantity = 0;
	if(costPriceFood > 0 && valueQuantity > 0){
		costFoodByQuantity = (parseFloat(valueQuantity) * parseFloat(costPriceFood));		
		costFoodByQuantity = costFoodByQuantity.toFixed(2);
	}

	//Calcular la venta por comida de acuerdo a la cantidad comida comprada con la cantidad ingresada
	var sellFoodByQuantity = 0;
	if(sellPriceFood > 0 && valueQuantity > 0){
		sellFoodByQuantity = (parseFloat(valueQuantity) * parseFloat(sellPriceFood));		
		sellFoodByQuantity = sellFoodByQuantity.toFixed(2);
	}

	labelCostFood.html('$ ' + costFoodByQuantity);
	labelCostFood.attr('cost-price-food-with-quantity', costFoodByQuantity);

	labelSellFood.html('$ ' + sellFoodByQuantity);
	labelSellFood.attr('sell-price-food-with-quantity', sellFoodByQuantity);

	//Seteamos el input costo total en variable y iniciamos variable para calculo
	var inputCostoTotal = $("#"+formId+" input[name=cost_price]");
	var costoTotal = 0;
	//Seteamos el input venta total en variable y iniciamos variable para calculo
	var inputVentaTotal = $("#"+formId+" input[name=sell_price]");
	var ventaTotal = 0;

	//Busco todos los labels con class cost-price-food-with-quantity
	//y al attr cost-price-food-with-quantitylo guardo
	$("#"+formId+ " [cost-price-food-with-quantity]").each(function(index, el) {
		costoTotal = parseFloat(costoTotal) + parseFloat($(el).attr('cost-price-food-with-quantity'));
	});
	
	//Busco todos los labels con class sell-price-food-with-quantity
	//y al attr sell-price-food-with-quantity lo guardo
	$("#"+formId+ " [sell-price-food-with-quantity]").each(function(index, el) {
		ventaTotal = parseFloat(ventaTotal) + parseFloat($(el).attr('sell-price-food-with-quantity'));
	});

	inputCostoTotal.val(costoTotal.toFixed(2));
	inputVentaTotal.val(ventaTotal.toFixed(2));

}

//Bloquear campos venta
function lockFieldsSaleOnEdit(){
	$("#formVenta [type=checkbox]").removeClass('chk-col-pink');
	$("#formVenta [type=checkbox]").addClass('chk-col-grey');
	$("#formVenta [type=checkbox]").prop('disabled', true);
	$("#formVenta .countButton").prop('disabled', true);
	$("#formVenta .quantityInput").prop('disabled', true);
	$("#formVenta input[name=cost_price]").prop('disabled', true);
	$("#formVenta #btn_unlock_foods").attr('disabled', false);
}

//Desbloquear campos venta
function unlockFieldsSaleOnEdit(){
	$("#formVenta [type=checkbox]").removeClass('chk-col-grey');
	$("#formVenta [type=checkbox]").addClass('chk-col-pink');
	$("#formVenta [type=checkbox]").removeAttr('disabled');
	$("#formVenta .countButton").removeAttr('disabled');
	$("#formVenta .quantityInput").removeAttr('disabled');
	$("#formVenta input[name=cost_price]").removeAttr('disabled');
	$("#formVenta #btn_unlock_foods").attr('disabled', true);
	$("#formVenta #btn_unlock_foods").attr('onclick', '');
}

//Inicializo a 0 los valores de impacto en producto para que no haya conflicots
function resetValueFood(){
	// Limpio los checks
	$("#formVenta [type=checkbox]").prop('checked', false);
	//Limpiar valores
	$("#formVenta .quantityInput").val(0);
	$("#formVenta label[cost-price-food-with-quantity]").attr('cost-price-food-with-quantity', '0').html('$ 0.00');
	$("#formVenta label[sell-price-food-with-quantity]").attr('sell-price-food-with-quantity', '0').html('$ 0.00');
	//Setear fecha hora momento
	var date_today = new Date;
	var date_today_str = date_today.toLocaleString();
	var date_today_formatted = formatDateTime(date_today_str ,'d-m-Y h:i');
	$("#formVenta input[name=sell_date]").val(date_today_str);
}

function deleteFoodsOfSale(id_sale){
	var route = root_project+"ventas_destroyFoodsBySale/"+id_sale;
    statusLoading('loading_modal', 1, 'Reseteando Comidas y Bebidas de la Venta ..');
    activeLoadingInButton("formVenta #btn_unlock_foods", 1, 'Desbloqueando y Reseteando ..');

	$.get(route, function(result){
		
		showMessageModal('Se resetearon las comidas y bebidas de la Venta. Seleccione las comidas y bebidas nuevamente ', 'Reseteo de Comidas y Bebidas Finalizado', 'bg-green', 'done');
		activeLoadingInButton("formVenta #btn_unlock_foods", 0, 'Desbloquear y Modificar Comidas y Bebidas');
		statusLoading('loading_modal', 0);
	});

}