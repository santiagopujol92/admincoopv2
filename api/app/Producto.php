<?php

namespace AdminCoopV2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Producto extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 		'productos';
    protected $fillable = 	['description', 
						    'id_type_product', 
						    'type_metric_product',
						    'value_product', 
						    'max_purchase_price',
						    'min_purchase_price',
						    'real_purchase_price', 
						    'last_purchase_price', 
						    'quantity_default_value', 
						    'quantity_accumulator_value', 
						    'stock', 
						    'last_change_stock'];
    protected $dates = 		['created_at', 'updated_at', 'deleted_at'];

}
