<?php

namespace AdminCoopV2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class SaleTypeProduct extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;
	
	protected $table = 'sale_type_products';
	protected $fillable = ['description'];
	protected $dates = ['deleted_at'];
}
