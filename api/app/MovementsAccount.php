<?php

namespace AdminCoopV2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class MovementsAccount extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'movements_accounts';
    protected $fillable = [
							'shipment_number', 
							'id_type_movement_account', 
							'id_current_account', 
							'id_sale_product',
							'id_people', 
							'quantity_sale_product' , 
							'sale_product_price', 
							'administrative_expenses', 
							'total_amount', 
							'date_movement'
						];
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

}
