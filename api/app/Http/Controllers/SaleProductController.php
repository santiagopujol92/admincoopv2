<?php

namespace AdminCoopV2\Http\Controllers;

use Illuminate\Http\Request;
use \AdminCoopV2\SaleProduct; 
use \AdminCoopV2\SaleTypeProduct; 
use Session;
use Redirect;

class SaleProductController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        //Permitir acceso para rol admin
        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'PRODUCTO VENTA';
        $this->modulo_msg = 'Producto Venta';
        $this->form = 'ProductoVenta';
        $this->module = 'productos_venta';
        $this->name_file = 'sale_product';
        $this->modals_btns = 'SaleProduct';
        $this->model = new SaleProduct;  
    }

    public function listing(){
        $data_controller = $this->model->join('sale_type_products', 'sale_products.id_sale_type_product', '=', 'sale_type_products.id')
            ->select('sale_products.id', 
                'sale_products.description',
                'sale_products.type_metric_product',
                'sale_products.id_sale_type_product',
        		'sale_type_products.description as sale_type_description')
            ->orderBy('sale_products.id', 'asc')
            ->get();
        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_sale_type_products = SaleTypeProduct::All();

        $data_controller = $this->model->join('sale_type_products', 'sale_products.id_sale_type_product', '=', 'sale_type_products.id')
            ->select('sale_products.id', 
                'sale_products.description',
                'sale_products.type_metric_product',
                'sale_products.id_sale_type_product',
                'sale_type_products.description as sale_type_description')
            ->orderBy('sale_products.id', 'asc')
            ->get();
        return view($this->module . '.' . $this->name_file . 's_index', compact('data_controller', 'data_sale_type_products'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . 's_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->find($id);

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All());
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'     
        ]);

    }
    
    //Buscar producto venta por tipo de producto venta
    function findBySaleTypeProductId($id){
        $products = $this->model->join('sale_type_products as stp', 'sale_products.id_sale_type_product', '=', 'stp.id')
            ->select('sale_products.id', 'sale_products.description')
            ->where('sale_products.id_sale_type_product', '=', $id)
            ->get();

        return response()->json(
            $products->toArray()
        );
    }

}
