<?php

namespace AdminCoopV2\Http\Controllers;

use Illuminate\Http\Request;
use \AdminCoopV2\Invoice; 
use \AdminCoopV2\StatusInvoice; 
use \AdminCoopV2\Person;
use \AdminCoopV2\LegalPerson;
use \AdminCoopV2\PaymentMethod;
use \AdminCoopV2\Producto;
use \AdminCoopV2\TypeProduct;
use \AdminCoopV2\TypePerson;
use Session;
use Redirect;
use \Carbon\Carbon;

class InvoiceController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct(){

        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        //Permitir acceso para rol admin
        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'FACTURA';
        $this->modulo_msg = 'Factura';
        $this->form = 'Factura';
        $this->module = 'facturas';
        $this->name_file = 'invoice';
        $this->modals_btns = 'Invoice';
        $this->model = new Invoice;

        //SQL PARA USAR EN EL INDEX Y EN EL LISTING PARA RENDERIZAR
        $this->sql_list = $this->model->join('status_invoices as sti', 'invoices.id_status_invoice', '=', 'sti.id')
            ->leftjoin('people as p', 'invoices.id_person', '=', 'p.id')
            ->leftjoin('legal_people as lp', 'invoices.id_legal_person', '=', 'lp.id')
            ->join('payment_methods as pm', 'invoices.id_payment_method', '=', 'pm.id')
            ->join('productos as pro', 'invoices.id_product', '=', 'pro.id')
            ->select('invoices.*', 'p.id as id_person', 'lp.id as id_legalpeople', 'sti.description as status_invoice', 'p.name as name', 'p.lastname as lastname', 'lp.name as business', 'pro.description as product')
            ->orderBy('invoices.created_at', 'desc')
            ->get();
    }

    public function listing(){
        $data_controller = $this->sql_list;
        return response()->json(
            $data_controller->toArray()          
        );
    }
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_controller = $this->sql_list;

        /* OBTENER DATA DE OTRA ENTIDAD A USAR EN LA VISTA, AGREGARLOS EN EL COMPACT*/
        $data_status_invoices = StatusInvoice::All();
        $data_people = Person::All(); //SOLO LLEVAR CLIEINTES
        $data_legal_people = LegalPerson::All();
        $data_payments_methods = PaymentMethod::All();
        $data_type_products = TypeProduct::All();
        $data_products = Producto::All();
        $data_type_persons = TypePerson::All();
        /**/    

        return view($this->module . '.' . $this->name_file . 's_index', compact('data_controller', 'data_status_invoices', 'data_legal_people','data_people', 'data_payments_methods', 'data_products', 'data_type_products', 'data_type_persons'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . 's_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carbon = new Carbon;

        if (isset($request->issue_date) && $request->issue_date != ''){
            // $request->issue_date = $carbon->parse($request->issue_date)->format('Y-m-d');
            $request['issue_date'] = $carbon->formatDateToSaveBD($request->issue_date);
        }
        if (isset($request->effective_date) && $request->effective_date != ''){
            // $request->effective_date = $carbon->parse($request->effective_date)->format('Y-m-d');
            $request['effective_date'] = $carbon->formatDateToSaveBD($request->effective_date);
        }
        if (isset($request->payment_date) && $request->payment_date != ''){
            // $request->payment_date = $carbon->parse($request->payment_date)->format('Y-m-d');
            $request['payment_date'] = $carbon->formatDateToSaveBD($request->payment_date);
        }

        if($request->paid != '' || $request->paid != null){
            $request['paid'] = '1';
        }else{
            $request['paid'] = '0';
        }

        if ($request->id_person == null || $request->id_person = ''){
            $request['id_person'] = 1;
        }

        if ($request->id_legal_person == null || $request->id_legal_person = ''){
            $request['id_legal_person'] = 1;
        }

        if($request->ajax()){
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->join('status_invoices as sti', 'invoices.id_status_invoice', '=', 'sti.id')
            ->leftjoin('people as p', 'invoices.id_person', '=', 'p.id')
            ->leftjoin('legal_people as lp', 'invoices.id_legal_person', '=', 'lp.id')
            ->join('productos as pro', 'invoices.id_product', '=', 'pro.id')
            ->select('invoices.*', 'p.id_type_people as id_type_people_person', 'lp.id_type_people as id_type_people_business', 'pro.type as id_type_product')
            ->where('invoices.id', '=', $id)
            ->get();

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $carbon = new Carbon;

        if (isset($request->issue_date) && $request->issue_date != '' && $request->issue_date != null && $request->issue_date != '0000-00-00'){
            // $request->issue_date = $carbon->parse($request->issue_date)->format('Y-m-d');
            $request['issue_date'] = $carbon->formatDateToSaveBD($request->issue_date);
        }else{
            $request['issue_date'] = '';
        }

        if (isset($request->effective_date) && $request->effective_date != '' && $request->effective_date != null && $request->effective_date != '0000-00-00'){
            // $request->effective_date = $carbon->parse($request->effective_date)->format('Y-m-d');
            $request['effective_date'] = $carbon->formatDateToSaveBD($request->effective_date);
        }else{
            $request['effective_date'] = '';
        }

        if (isset($request->payment_date) && $request->payment_date != '' && $request->payment_date != null && $request->payment_date != '0000-00-00'){
            // $request->payment_date = $carbon->parse($request->payment_date)->format('Y-m-d');
            $request['payment_date'] = $carbon->formatDateToSaveBD($request->payment_date);
        }else{
            $request['payment_date'] = '';
        }


        if($request->paid != '' || $request->paid != null){
            $request['paid'] = '1';
        }else{
            $request['paid'] = '0';
        }

        if ($request->id_person == null || $request->id_person = ''){
            $request['id_person'] = 1;
        }

        if ($request->id_legal_person == null || $request->id_legal_person = ''){
            $request['id_legal_person'] = 1;
        }

        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All()); //Rellena el elemento usuario con fill
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'      
        ]);

    }

}