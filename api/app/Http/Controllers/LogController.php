<?php

namespace AdminCoopV2\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;
use \AdminCoopV2\User;

class LogController extends Controller
{
    private $model;
    private $modulo_msg;

    //CORROBORAR AUTENTIFICACION
    public function __construct()
    {
        //Permitir acceso siempre autenticado
        // $this->middleware('auth');

        // //Permitir acceso para rol root
        // $this->middleware('root');
        $this->modulo_msg = 'Usuario';
        $this->model = new User;
    }

    public function logout(){
        Auth::logout();
        return Redirect::to('/');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return null;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // METHOD POST
    public function store(Request $request)
    {
        $mensaje = '';
        $status = 0;
        $data = [];

        $user = $this->model->join('type_users', 'users.type', '=', 'type_users.id')
            ->select('users.id', 'users.type', 'users.username', 'users.name', 'users.lastname', 'users.password', 'users.email', 'users.status', 'users.created_at', 'type_users.description as type_description')
            ->where('users.username', '=', $request->username)
            ->where('users.password', '=', md5($request->password))
            ->get();

        if (count($user) == 0) {
            $mensaje = 'Usuario y/o Contraseña Incorrecta';
            $user = null;
            $status = 0;
        } else {
            if ($user[0]['status'] == 'on'){
                $mensaje = 'Usuario Logeado con Éxito';
                $status = 1;
            }else{
                $mensaje = 'Usuario Deshabilitado';
                $status = -1;
            }
        }
        return response()->json([
            'mensaje' => $mensaje,
            'data' => $user,
            'estado' => $status
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
