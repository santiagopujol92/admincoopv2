<?php

namespace AdminCoopV2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Invoice extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'invoices';

    protected $fillable = [
					    	'invoice_number', 
					    	'id_person', 
					    	'total_amount',
					    	'parcial_amount',
					    	'fees',
					    	'administrative_expenses',
					    	'id_payment_method', 
					    	'id_product', 
					    	'id_status_invoice', 
					    	'issue_date',
					    	'effective_date',
					    	'payment_date',
					    	'paid'
				   		];

    protected $dates = ['deleted_at'];
}
