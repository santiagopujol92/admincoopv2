<?php

namespace AdminCoopV2;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class SaleProduct extends Model implements AuditableContract
{

	use SoftDeletes, Auditable;

    protected $table = 		'sale_products';
    protected $fillable = 	[
							'description', 
						    'id_sale_type_product', 
						    'type_metric_product'
							];

    protected $dates = 		['created_at', 'updated_at', 'deleted_at'];
}
