-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-01-2019 a las 05:18:34
-- Versión del servidor: 10.1.22-MariaDB
-- Versión de PHP: 7.0.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `admincoop_barrancayaco`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audits`
--

CREATE TABLE `audits` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_id` int(10) UNSIGNED NOT NULL,
  `auditable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_values` text COLLATE utf8mb4_unicode_ci,
  `new_values` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `audits`
--

INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(1, 27, 'updated', 27, 'AdminCoop\\User', '{\"remember_token\":\"vaMguckkKovU9kTcsnSBuqQNOIV2x91cwjh7k7sm1GSrpielzbXvW6TdQBxI\"}', '{\"remember_token\":\"dwltLAaez3vTg3uAOcHjKXjPHJB1pAfw8lxDmACxtjzfENHoJIN6bRj1rwYN\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:16:01'),
(2, 26, 'updated', 26, 'AdminCoop\\User', '{\"remember_token\":\"2dhgF6m7p28zrMUB51qTjNV6RzI8RdfXfzGsGkq6R0rW9eNjIwBzcYhOiURs\"}', '{\"remember_token\":\"0Z2zCgelooWsyZiZg8VUc4oLxihU6pgcKhG1UQt2T1sNsG9klfkjFdC1SRrV\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:16:21'),
(3, 26, 'updated', 26, 'AdminCoop\\User', '{\"remember_token\":\"0Z2zCgelooWsyZiZg8VUc4oLxihU6pgcKhG1UQt2T1sNsG9klfkjFdC1SRrV\"}', '{\"remember_token\":\"3rR03rEenF8btpVyS71YKeJH8zAeEpNv7Nnt6aUc31msAWP9bWK2JmR24GZu\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:16:28'),
(4, 27, 'updated', 27, 'AdminCoop\\User', '{\"remember_token\":\"dwltLAaez3vTg3uAOcHjKXjPHJB1pAfw8lxDmACxtjzfENHoJIN6bRj1rwYN\"}', '{\"remember_token\":\"1iVLuBPbyyplHHTaQA4rkYTScGMY8KGFftZ8QfbvxO0YoaD1rcwGlsYS27Jp\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:17:07'),
(5, 27, 'updated', 27, 'AdminCoop\\User', '{\"remember_token\":\"1iVLuBPbyyplHHTaQA4rkYTScGMY8KGFftZ8QfbvxO0YoaD1rcwGlsYS27Jp\"}', '{\"remember_token\":\"3XgRG5NbHdNMzrhsrLtBVOQLDJueMnuGJuGSL8GBlNmPfmJsFXgxPXcx9wgf\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:17:20'),
(6, 26, 'updated', 26, 'AdminCoop\\User', '{\"remember_token\":\"3rR03rEenF8btpVyS71YKeJH8zAeEpNv7Nnt6aUc31msAWP9bWK2JmR24GZu\"}', '{\"remember_token\":\"9bu8nSdPleo3uou0J8fFv0dgU1s3cMuC0y1UOXSHCuQNGgwqMgfOOr233rab\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:17:41'),
(7, 27, 'created', 8, 'AdminCoop\\StatusShipment', '[]', '{\"description\":\"Test\",\"id\":8}', 'http://localhost:8000/estados_remitos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:04:22'),
(8, 27, 'deleted', 8, 'AdminCoop\\StatusShipment', '{\"id\":8,\"description\":\"Test\",\"deleted_at\":\"2019-01-11 22:04:25\"}', '[]', 'http://localhost:8000/estados_remitos/8', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:04:25'),
(9, 27, 'created', 3, 'AdminCoop\\CondicionIva', '[]', '{\"description\":\"Monotributo\",\"id\":3}', 'http://localhost:8000/condiciones_ivas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:05:07'),
(10, 27, 'created', 4, 'AdminCoop\\CondicionIva', '[]', '{\"description\":\"Responsable Inscripto\",\"id\":4}', 'http://localhost:8000/condiciones_ivas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:05:15'),
(11, 27, 'updated', 39, 'AdminCoop\\TypeProduct', '{\"description\":\"Gramos\"}', '{\"description\":\"Tipo 1\"}', 'http://localhost:8000/tipo_productos/39', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:07:48'),
(12, 27, 'updated', 40, 'AdminCoop\\TypeProduct', '{\"description\":\"Unidad\\/es\"}', '{\"description\":\"Tipo 2\"}', 'http://localhost:8000/tipo_productos/40', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:07:52'),
(13, 27, 'created', 2, 'AdminCoop\\Person', '[]', '{\"name\":\"Santiago\",\"lastname\":\"Pujol\",\"email\":\"santiagopujol92@gmail.com\",\"cuit\":\"20363548459\",\"adress\":\"Chascomus 1782\",\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"id_condicion_iva\":\"3\",\"personality\":\"F\",\"id_type_people\":\"1\",\"id_city\":\"1\",\"id\":2}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-14 04:07:51'),
(14, 27, 'updated', 2, 'AdminCoop\\Person', '{\"personality\":\"F\"}', '{\"personality\":\"J\"}', 'http://localhost:8000/personas/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-14 04:09:35'),
(15, 27, 'updated', 1, 'AdminCoop\\Person', '{\"id_condicion_iva\":1}', '{\"id_condicion_iva\":\"3\"}', 'http://localhost:8000/personas/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-14 04:13:22'),
(16, 27, 'updated', 39, 'AdminCoop\\TypeProduct', '{\"description\":\"Tipo 1\"}', '{\"description\":\"Duro\"}', 'http://localhost:8000/tipo_productos/39', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-14 23:16:19'),
(17, 27, 'created', 2, 'AdminCoop\\Person', '[]', '{\"name\":\"Santiago\",\"lastname\":\"Pujol Cliente\",\"email\":\"santiagopujol93@gmail.com\",\"cuit\":\"20363548456\",\"adress\":\"San Carlos\",\"floor\":null,\"department\":null,\"phone_1\":\"34234324234\",\"phone_2\":null,\"id_condicion_iva\":\"2\",\"personality\":\"F\",\"id_type_people\":\"1\",\"id_city\":\"1\",\"id\":2}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-15 03:38:54'),
(18, 27, 'updated', 1, 'AdminCoop\\Person', '{\"lastname\":\"Pujol\"}', '{\"lastname\":\"Pujol Proveedor\"}', 'http://localhost:8000/personas/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-15 03:39:02'),
(19, 27, 'created', 3, 'AdminCoop\\Person', '[]', '{\"name\":\"Santiago\",\"lastname\":\"Pujol Empleado\",\"email\":\"santiagopujol52@gmail.com\",\"cuit\":\"20363548454\",\"adress\":\"Chascomus 1782\",\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"id_condicion_iva\":\"1\",\"personality\":\"F\",\"id_type_people\":\"3\",\"id_city\":\"1\",\"id\":3}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-15 03:39:32'),
(20, 27, 'created', 4, 'AdminCoop\\Person', '[]', '{\"name\":\"Santiago\",\"lastname\":\"Pujol Proveedor Empresa\",\"email\":\"santiagopuj2@gmail.com\",\"cuit\":\"234234324\",\"adress\":\"43534531782\",\"floor\":null,\"department\":null,\"phone_1\":\"234234234\",\"phone_2\":null,\"id_condicion_iva\":\"2\",\"personality\":\"J\",\"id_type_people\":\"2\",\"id_city\":\"1\",\"id\":4}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-15 03:40:30'),
(21, 27, 'created', 1, 'AdminCoop\\Producto', '[]', '{\"description\":\"BIDON\",\"id_type_product\":\"40\",\"type_metric_product\":\"KG\",\"min_purchase_price\":null,\"max_purchase_price\":null,\"last_purchase_price\":null,\"id\":1}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:05:10'),
(22, 27, 'updated', 1, 'AdminCoop\\Producto', '{\"max_purchase_price\":null,\"min_purchase_price\":null,\"last_purchase_price\":null}', '{\"max_purchase_price\":\"4\",\"min_purchase_price\":\"3\",\"last_purchase_price\":\"5\"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:15:08'),
(23, 27, 'updated', 1, 'AdminCoop\\Producto', '{\"max_purchase_price\":4}', '{\"max_purchase_price\":\"7\"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:15:49'),
(24, 27, 'updated', 1, 'AdminCoop\\Producto', '{\"max_purchase_price\":7}', '{\"max_purchase_price\":\"7.33\"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:15:56'),
(25, 27, 'updated', 1, 'AdminCoop\\Producto', '{\"min_purchase_price\":3,\"last_purchase_price\":5}', '{\"min_purchase_price\":\"3.50\",\"last_purchase_price\":\"5.6\"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:16:05'),
(26, 27, 'updated', 1, 'AdminCoop\\Producto', '{\"min_purchase_price\":3.5}', '{\"min_purchase_price\":\"3.50\"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:16:05'),
(27, 27, 'updated', 40, 'AdminCoop\\TypeProduct', '{\"description\":\"Tipo 2\"}', '{\"description\":\"SILO\"}', 'http://localhost:8000/tipo_productos/40', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:17:04'),
(28, 27, 'updated', 39, 'AdminCoop\\TypeProduct', '{\"description\":\"Duro\"}', '{\"description\":\"DURO\"}', 'http://localhost:8000/tipo_productos/39', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:17:11'),
(29, 27, 'created', 1, 'AdminCoop\\SaleTypeProduct', '[]', '{\"description\":\"SOPLADO GRANDE\",\"id\":1}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:28:54'),
(30, 27, 'created', 2, 'AdminCoop\\SaleTypeProduct', '[]', '{\"description\":\"SOPLADO CHICO\",\"id\":2}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:29:04'),
(31, 27, 'created', 3, 'AdminCoop\\SaleTypeProduct', '[]', '{\"description\":\"PP\",\"id\":3}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:29:24'),
(32, 27, 'created', 4, 'AdminCoop\\SaleTypeProduct', '[]', '{\"description\":\"PEBO\",\"id\":4}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:29:33'),
(33, 27, 'created', 5, 'AdminCoop\\SaleTypeProduct', '[]', '{\"description\":\"ROTOMOLDEO (TODO TIPO)\",\"id\":5}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:29:46'),
(34, 27, 'created', 6, 'AdminCoop\\SaleTypeProduct', '[]', '{\"description\":\"PEAD CAJON MOLIDO\",\"id\":6}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:29:58'),
(35, 27, 'created', 7, 'AdminCoop\\SaleTypeProduct', '[]', '{\"description\":\"PEAD MOL\",\"id\":7}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:30:12'),
(36, 27, 'created', 8, 'AdminCoop\\SaleTypeProduct', '[]', '{\"description\":\"SILLA MOL\",\"id\":8}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:30:18'),
(37, 27, 'created', 1, 'AdminCoop\\SaleProduct', '[]', '{\"description\":\"Blanco\",\"id_sale_type_product\":\"2\",\"type_metric_product\":\"KG\",\"id\":1}', 'http://localhost:8000/productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 02:11:06'),
(38, 27, 'created', 2, 'AdminCoop\\SaleProduct', '[]', '{\"description\":\"Rojo\",\"id_sale_type_product\":\"3\",\"type_metric_product\":\"KG\",\"id\":2}', 'http://localhost:8000/productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 02:11:26'),
(39, 27, 'updated', 2, 'AdminCoop\\SaleProduct', '{\"id_sale_type_product\":3}', '{\"id_sale_type_product\":\"6\"}', 'http://localhost:8000/productos_venta/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 02:11:37'),
(40, 27, 'updated', 1, 'AdminCoop\\SaleProduct', '{\"id_sale_type_product\":2}', '{\"id_sale_type_product\":\"6\"}', 'http://localhost:8000/productos_venta/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 02:11:51'),
(41, 27, 'updated', 2, 'AdminCoop\\SaleProduct', '{\"id_sale_type_product\":6}', '{\"id_sale_type_product\":\"8\"}', 'http://localhost:8000/productos_venta/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 02:11:58'),
(42, 27, 'created', 1, 'AdminCoop\\TypeMovementAccount', '[]', '{\"description\":\"DEBITO\",\"id\":1}', 'http://localhost:8000/tipo_movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-21 04:05:52'),
(43, 27, 'created', 2, 'AdminCoop\\TypeMovementAccount', '[]', '{\"description\":\"CREDITO\",\"id\":2}', 'http://localhost:8000/tipo_movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-21 04:05:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_province` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`id`, `description`, `id_province`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Córdoba', 1, '2017-08-21 09:09:37', '2017-08-21 09:11:54', NULL),
(2, 'Alta Gracia', 1, '2017-08-21 09:10:41', '2017-08-21 09:10:41', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condicion_ivas`
--

CREATE TABLE `condicion_ivas` (
  `id` int(11) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `condicion_ivas`
--

INSERT INTO `condicion_ivas` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Monotributo', '2017-08-02 06:13:04', '2017-11-18 23:50:42', NULL),
(2, 'Responsable Inscripto', '2017-08-02 06:13:15', '2017-11-18 23:50:40', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Argentina', '2017-08-21 08:19:03', '2017-08-21 08:21:15', NULL),
(2, 'Brazil', '2017-08-21 08:19:31', '2017-08-21 08:19:31', NULL),
(3, 'Venezuela', '2017-08-21 08:19:33', '2017-08-21 08:19:33', NULL),
(4, 'Bolivia', '2017-08-21 08:19:35', '2017-08-21 08:19:35', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `current_accounts`
--

CREATE TABLE `current_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_person` int(10) UNSIGNED NOT NULL,
  `balance` double(12,2) DEFAULT NULL,
  `id_status_current_account` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_number` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_person` int(10) UNSIGNED DEFAULT NULL,
  `total_amount` double(12,2) DEFAULT NULL,
  `parcial_amount` double(12,2) DEFAULT NULL,
  `fees` int(11) DEFAULT NULL,
  `administrative_expenses` double(12,2) DEFAULT NULL,
  `id_payment_method` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_status_invoice` int(10) UNSIGNED NOT NULL,
  `issue_date` date NOT NULL,
  `effective_date` date NOT NULL,
  `payment_date` date DEFAULT NULL,
  `paid` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `invoices`
--

INSERT INTO `invoices` (`id`, `invoice_number`, `id_person`, `total_amount`, `parcial_amount`, `fees`, `administrative_expenses`, `id_payment_method`, `id_product`, `id_status_invoice`, `issue_date`, `effective_date`, `payment_date`, `paid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, '4356543', 1, 54654.00, NULL, NULL, NULL, 2, 29, 4, '2017-09-02', '2017-09-02', NULL, '0', '2017-09-03 03:12:05', '2017-09-03 03:12:05', NULL),
(10, '435345', 1, 345345.00, NULL, NULL, NULL, 2, 28, 4, '2017-09-06', '2017-09-06', NULL, '0', '2017-09-06 06:23:34', '2017-09-06 06:23:34', NULL),
(11, '34534', 1, 345435.00, NULL, NULL, NULL, 3, 28, 4, '2017-09-06', '2017-09-06', NULL, '0', '2017-09-06 08:36:34', '2017-09-06 08:36:34', NULL),
(12, '35665', 1, 1525252.00, NULL, NULL, NULL, 4, 28, 4, '2017-09-09', '2017-09-10', NULL, '0', '2017-09-09 03:08:32', '2017-09-09 03:08:32', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_07_14_112334_create_productos_table', 1),
(2, '2019_01_17_221809_create_sale_type_products_table', 2),
(3, '2019_01_17_224118_create_sale_products_table', 3),
(4, '2019_01_17_224120_create_sale_products_table', 4),
(5, '2019_01_21_000516_status_current_account', 5),
(6, '2019_01_20_236600_create_current_accounts_table', 6),
(7, '2019_01_21_001638_create_type_movement_accounts_table', 7),
(8, '2019_01_21_001639_create_type_movement_accounts_table', 8),
(9, '2019_01_21_001645_create_type_movement_accounts_table', 9),
(10, '2019_01_21_001058_create_movements_accounts_table', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movements_accounts`
--

CREATE TABLE `movements_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `shipment_number` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_type_movement_account` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_current_account` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_sale_product` int(10) UNSIGNED NOT NULL,
  `id_people` int(10) UNSIGNED NOT NULL,
  `quantity_sale_product` int(11) DEFAULT NULL,
  `sale_product_price` double(12,2) DEFAULT NULL,
  `administrative_expenses` double(12,2) DEFAULT NULL,
  `total_amount` double(12,2) NOT NULL,
  `date_movement` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@admin.com', '$2y$10$.6R/srIeNZguf04qZw1Wnu6RQOof3D9tAJM9pqz6VUByPyK7MtilW', '2017-06-18 02:54:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Tarjeta', '2017-08-02 06:26:43', '2017-08-02 06:26:43', NULL),
(2, 'Efectivo', '2017-08-02 06:26:47', '2017-08-02 06:26:47', NULL),
(3, 'Debito', '2017-08-02 06:26:53', '2017-08-02 06:26:53', NULL),
(4, 'Cheque', '2017-08-02 06:26:57', '2017-08-02 06:26:57', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people`
--

CREATE TABLE `people` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuit` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_condicion_iva` int(10) UNSIGNED DEFAULT NULL,
  `id_type_people` int(10) UNSIGNED DEFAULT NULL,
  `id_city` int(10) UNSIGNED DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `floor` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personality` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `people`
--

INSERT INTO `people` (`id`, `name`, `lastname`, `email`, `cuit`, `id_condicion_iva`, `id_type_people`, `id_city`, `adress`, `floor`, `department`, `phone_1`, `phone_2`, `personality`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Santiago', 'Pujol Proveedor', 'santiagopujol92@gmail.com', '20363548459', 1, 2, 1, 'Chascomus 1782', NULL, NULL, '4643997', NULL, 'F', '2017-11-20 23:55:09', '2019-01-15 03:39:02', NULL),
(2, 'Santiago', 'Pujol Cliente', 'santiagopujol93@gmail.com', '20363548456', 2, 1, 1, 'San Carlos', NULL, NULL, '34234324234', NULL, 'F', '2019-01-15 03:38:54', '2019-01-15 03:38:54', NULL),
(3, 'Santiago', 'Pujol Empleado', 'santiagopujol52@gmail.com', '20363548454', 1, 3, 1, 'Chascomus 1782', NULL, NULL, NULL, NULL, 'F', '2019-01-15 03:39:32', '2019-01-15 03:39:32', NULL),
(4, 'Santiago', 'Pujol Proveedor Empresa', 'santiagopuj2@gmail.com', '234234324', 2, 2, 1, '43534531782', NULL, NULL, '234234234', NULL, 'J', '2019-01-15 03:40:30', '2019-01-15 03:40:30', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_type_product` int(10) UNSIGNED NOT NULL,
  `type_metric_product` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'KG',
  `value_product` double(10,2) DEFAULT NULL,
  `max_purchase_price` double(10,2) DEFAULT NULL,
  `min_purchase_price` double(10,2) DEFAULT NULL,
  `real_purchase_price` double(10,2) DEFAULT NULL,
  `last_purchase_price` double(10,2) DEFAULT NULL,
  `quantity_default_value` double(10,2) DEFAULT NULL,
  `quantity_accumulator_value` double(10,2) DEFAULT NULL,
  `stock` double(10,2) DEFAULT NULL,
  `last_change_stock` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `description`, `id_type_product`, `type_metric_product`, `value_product`, `max_purchase_price`, `min_purchase_price`, `real_purchase_price`, `last_purchase_price`, `quantity_default_value`, `quantity_accumulator_value`, `stock`, `last_change_stock`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BIDON', 40, 'KG', NULL, 7.33, 3.50, NULL, 5.60, NULL, NULL, NULL, NULL, '2019-01-17 03:05:10', '2019-01-17 03:16:05', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provinces`
--

CREATE TABLE `provinces` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `provinces`
--

INSERT INTO `provinces` (`id`, `description`, `id_country`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cordoba', 1, '2017-08-21 08:28:45', '2017-08-22 06:17:29', NULL),
(2, 'Sao Pablo', 2, '2017-08-21 08:29:37', '2017-08-22 06:17:34', NULL),
(3, 'Tucuman', 1, '2017-08-21 08:31:51', '2017-08-22 06:17:45', NULL),
(5, 'Buenos Aires', 1, '2017-08-22 06:15:00', '2017-08-22 06:17:40', NULL),
(7, 'Caracas', 3, '2017-08-22 06:17:54', '2017-08-22 06:17:54', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_person` int(10) NOT NULL,
  `id_person_employee` int(10) NOT NULL,
  `cost_price` double(8,2) NOT NULL,
  `sell_price` double(8,2) NOT NULL,
  `sell_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sale_products`
--

CREATE TABLE `sale_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_sale_type_product` int(10) UNSIGNED NOT NULL,
  `type_metric_product` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'KG',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sale_products`
--

INSERT INTO `sale_products` (`id`, `description`, `id_sale_type_product`, `type_metric_product`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Blanco', 6, 'KG', '2019-01-18 02:11:06', '2019-01-18 02:11:51', NULL),
(2, 'Rojo', 8, 'KG', '2019-01-18 02:11:26', '2019-01-18 02:11:58', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sale_type_products`
--

CREATE TABLE `sale_type_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sale_type_products`
--

INSERT INTO `sale_type_products` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SOPLADO GRANDE', '2019-01-18 01:28:54', '2019-01-18 01:28:54', NULL),
(2, 'SOPLADO CHICO', '2019-01-18 01:29:04', '2019-01-18 01:29:04', NULL),
(3, 'PP', '2019-01-18 01:29:24', '2019-01-18 01:29:24', NULL),
(4, 'PEBO', '2019-01-18 01:29:33', '2019-01-18 01:29:33', NULL),
(5, 'ROTOMOLDEO (TODO TIPO)', '2019-01-18 01:29:46', '2019-01-18 01:29:46', NULL),
(6, 'PEAD CAJON MOLIDO', '2019-01-18 01:29:58', '2019-01-18 01:29:58', NULL),
(7, 'PEAD MOL', '2019-01-18 01:30:12', '2019-01-18 01:30:12', NULL),
(8, 'SILLA MOL', '2019-01-18 01:30:18', '2019-01-18 01:30:18', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_current_account`
--

CREATE TABLE `status_current_account` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_invoices`
--

CREATE TABLE `status_invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status_invoices`
--

INSERT INTO `status_invoices` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 'Activa', '2017-08-27 02:04:41', '2017-09-03 01:38:59', NULL),
(6, 'Anulada', '2017-08-27 02:04:50', '2017-08-27 02:04:50', NULL),
(7, 'Finalizada', '2017-09-03 01:39:19', '2017-09-03 01:39:19', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_shipments`
--

CREATE TABLE `status_shipments` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status_shipments`
--

INSERT INTO `status_shipments` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Activo', '2017-08-27 02:03:12', '2017-08-27 02:03:31', NULL),
(2, 'Pendiente de Pago', '2017-08-27 02:03:15', '2017-08-27 02:04:27', NULL),
(3, 'Pagado', '2017-08-27 02:03:20', '2017-08-27 02:06:32', NULL),
(4, 'A confirmar', '2017-08-27 02:06:40', '2017-08-27 02:06:40', NULL),
(5, 'Preparado Para Facturar', '2017-08-27 02:06:59', '2017-08-27 02:06:59', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_movement_accounts`
--

CREATE TABLE `type_movement_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_movement_accounts`
--

INSERT INTO `type_movement_accounts` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'DEBITO', '2019-01-21 04:05:52', '2019-01-21 04:05:52', NULL),
(2, 'CREDITO', '2019-01-21 04:05:58', '2019-01-21 04:05:58', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_people`
--

CREATE TABLE `type_people` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_people`
--

INSERT INTO `type_people` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CLIENTE', '2017-08-03 06:53:57', '2017-08-03 06:53:57', NULL),
(2, 'PROVEEDOR', '2017-08-03 06:54:43', '2017-08-03 06:54:43', NULL),
(3, 'EMPLEADO', '2017-08-03 06:55:05', '2017-08-09 08:25:15', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_products`
--

CREATE TABLE `type_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_products`
--

INSERT INTO `type_products` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(39, 'DURO', '2017-11-08 08:50:04', '2019-01-17 03:17:11', NULL),
(40, 'SILO', '2017-11-08 08:50:18', '2019-01-17 03:17:04', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_users`
--

CREATE TABLE `type_users` (
  `id` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_users`
--

INSERT INTO `type_users` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'Root', '2017-11-12 21:44:45', '2019-01-10 22:47:00', NULL),
(3, 'Carga Datos', '2019-01-10 22:46:52', '2019-01-10 22:46:52', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `status` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `password`, `type`, `status`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(26, 'Admin', 'Admin', 'admin', '$2y$10$DSf4JEQwb0LuC.S10Y.gH.tPgBW7wXarmKUaN0Zyfyi7X3zj4PSy.', 1, 'on', '9bu8nSdPleo3uou0J8fFv0dgU1s3cMuC0y1UOXSHCuQNGgwqMgfOOr233rab', NULL, NULL, NULL),
(27, 'Root', 'Root', 'root', '$2y$10$DSf4JEQwb0LuC.S10Y.gH.tPgBW7wXarmKUaN0Zyfyi7X3zj4PSy.', 2, 'on', '3XgRG5NbHdNMzrhsrLtBVOQLDJueMnuGJuGSL8GBlNmPfmJsFXgxPXcx9wgf', '2017-11-12 21:45:14', '2017-11-12 21:45:14', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_id_province_foreign` (`id_province`);

--
-- Indices de la tabla `condicion_ivas`
--
ALTER TABLE `condicion_ivas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `current_accounts`
--
ALTER TABLE `current_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `current_accounts_id_person_foreign` (`id_person`),
  ADD KEY `current_accounts_id_status_current_account_foreign` (`id_status_current_account`);

--
-- Indices de la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoices_invoice_number_unique` (`invoice_number`),
  ADD KEY `invoices_id_person_foreign` (`id_person`),
  ADD KEY `invoices_id_payment_method_foreign` (`id_payment_method`),
  ADD KEY `invoices_id_product_foreign` (`id_product`),
  ADD KEY `invoices_id_status_invoice_foreign` (`id_status_invoice`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `movements_accounts`
--
ALTER TABLE `movements_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `movements_accounts_shipment_number_unique` (`shipment_number`),
  ADD KEY `movements_accounts_id_type_movement_account_foreign` (`id_type_movement_account`),
  ADD KEY `movements_accounts_id_current_account_foreign` (`id_current_account`),
  ADD KEY `movements_accounts_id_sale_product_foreign` (`id_sale_product`),
  ADD KEY `movements_accounts_id_people_foreign` (`id_people`);

--
-- Indices de la tabla `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `people_cuit_unique` (`cuit`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `people_id_condicion_iva_foreign` (`id_condicion_iva`),
  ADD KEY `people_id_type_people_foreign` (`id_type_people`),
  ADD KEY `id_city` (`id_city`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productos_id_type_product_foreign` (`id_type_product`);

--
-- Indices de la tabla `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinces_id_country_foreign` (`id_country`);

--
-- Indices de la tabla `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_id_person_foreign` (`id_person`);

--
-- Indices de la tabla `sale_products`
--
ALTER TABLE `sale_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_products_id_sale_type_product_foreign` (`id_sale_type_product`);

--
-- Indices de la tabla `sale_type_products`
--
ALTER TABLE `sale_type_products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_current_account`
--
ALTER TABLE `status_current_account`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_invoices`
--
ALTER TABLE `status_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_shipments`
--
ALTER TABLE `status_shipments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_movement_accounts`
--
ALTER TABLE `type_movement_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_people`
--
ALTER TABLE `type_people`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_products`
--
ALTER TABLE `type_products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_users`
--
ALTER TABLE `type_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_type_foreign` (`type`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `audits`
--
ALTER TABLE `audits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `condicion_ivas`
--
ALTER TABLE `condicion_ivas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `current_accounts`
--
ALTER TABLE `current_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `movements_accounts`
--
ALTER TABLE `movements_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `people`
--
ALTER TABLE `people`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=367;
--
-- AUTO_INCREMENT de la tabla `sale_products`
--
ALTER TABLE `sale_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `sale_type_products`
--
ALTER TABLE `sale_type_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `status_current_account`
--
ALTER TABLE `status_current_account`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `status_invoices`
--
ALTER TABLE `status_invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `status_shipments`
--
ALTER TABLE `status_shipments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `type_movement_accounts`
--
ALTER TABLE `type_movement_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `type_people`
--
ALTER TABLE `type_people`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `type_products`
--
ALTER TABLE `type_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_id_province_foreign` FOREIGN KEY (`id_province`) REFERENCES `provinces` (`id`);

--
-- Filtros para la tabla `current_accounts`
--
ALTER TABLE `current_accounts`
  ADD CONSTRAINT `current_accounts_id_person_foreign` FOREIGN KEY (`id_person`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `current_accounts_id_status_current_account_foreign` FOREIGN KEY (`id_status_current_account`) REFERENCES `status_current_account` (`id`);

--
-- Filtros para la tabla `movements_accounts`
--
ALTER TABLE `movements_accounts`
  ADD CONSTRAINT `movements_accounts_id_current_account_foreign` FOREIGN KEY (`id_current_account`) REFERENCES `current_accounts` (`id`),
  ADD CONSTRAINT `movements_accounts_id_people_foreign` FOREIGN KEY (`id_people`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `movements_accounts_id_sale_product_foreign` FOREIGN KEY (`id_sale_product`) REFERENCES `sale_products` (`id`),
  ADD CONSTRAINT `movements_accounts_id_type_movement_account_foreign` FOREIGN KEY (`id_type_movement_account`) REFERENCES `type_movement_accounts` (`id`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_id_type_product_foreign` FOREIGN KEY (`id_type_product`) REFERENCES `type_products` (`id`);

--
-- Filtros para la tabla `provinces`
--
ALTER TABLE `provinces`
  ADD CONSTRAINT `provinces_id_country_foreign` FOREIGN KEY (`id_country`) REFERENCES `countries` (`id`);

--
-- Filtros para la tabla `sale_products`
--
ALTER TABLE `sale_products`
  ADD CONSTRAINT `sale_products_id_sale_type_product_foreign` FOREIGN KEY (`id_sale_type_product`) REFERENCES `sale_type_products` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_type_foreign` FOREIGN KEY (`type`) REFERENCES `type_users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
