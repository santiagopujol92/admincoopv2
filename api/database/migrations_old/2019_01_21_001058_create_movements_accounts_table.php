<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementsAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movements_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shipment_number', 100)->unique();
            $table->integer('id_type_movement_account')->unsigned()->default(0);
            $table->foreign('id_type_movement_account')->references('id')->on('type_movement_accounts');
            $table->integer('id_current_account')->unsigned()->default(0);
            $table->foreign('id_current_account')->references('id')->on('current_accounts');
            $table->integer('id_sale_product')->unsigned();
            $table->foreign('id_sale_product')->references('id')->on('sale_products');
            $table->integer('id_people')->unsigned();
            $table->foreign('id_people')->references('id')->on('people');
            $table->integer('quantity_sale_product')->nullable();
            $table->float('sale_product_price', 12, 2)->nullable();
            $table->float('administrative_expenses', 12, 2)->nullable();
            $table->float('total_amount', 12, 2);
            $table->date('date_movement');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements_accounts');
    }
}
