<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_number', 100)->unique();
            $table->integer('id_person')->unsigned()->default(0);
            $table->foreign('id_person')->references('id')->on('people');
            $table->float('total_amount', 12, 2)->nullable();
            $table->float('parcial_amount', 12, 2)->nullable();
            $table->integer('fees')->nullable();
            $table->float('administrative_expenses', 12, 2)->nullable();
            $table->integer('id_payment_method')->unsigned()->default(0);
            $table->foreign('id_payment_method')->references('id')->on('payment_methods');
            $table->integer('id_product')->unsigned();
            $table->foreign('id_product')->references('id')->on('productos');
            $table->integer('id_status_invoice')->unsigned();
            $table->foreign('id_status_invoice')->references('id')->on('status_invoices');
            $table->date('issue_date');
            $table->date('effective_date')->nullable();;
            $table->date('payment_date')->nullable();
            $table->char('paid', 1)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
