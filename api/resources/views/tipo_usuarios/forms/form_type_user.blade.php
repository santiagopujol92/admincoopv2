<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">format_list_numbered</i>
    </span>
    <div class="form-line">
        {!! Form::text('id', null, ['class' => 'form-control', 'msg' => 'Número Identificador', 'placeholder' => "Ingrese Numero de " . $modulo_msg, 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">person</i>
    </span>
    <div class="form-line">
        {!! Form::text('description', null, ['class' => 'form-control', 'msg' => 'Nombre', 'placeholder' => "Ingrese Nombre " . $modulo_msg, 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>