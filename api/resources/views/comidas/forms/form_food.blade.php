<label for="description">Nombre:</label>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        {!! Form::text('description', null, ['class' => 'form-control', 'msg' => 'Nombre', 'placeholder' => "Ingrese Nombre " . $modulo_msg, 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>

{{-- <label for="status">Estado:</label>
<div class="input-group input-above3">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        <select name="status" class="form-control show-tick" msg='Estado' required data-live-search="true" >
            <option value="0">Seleccione Estado Comida o Bebida</option>
            <option value="1">Disponible</option>
            <option value="2">No Disponible</option>
        </select>
    </div>
</div> --}}

<label for="type">Tipo:</label>
<div class="input-group input-above2">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        <select name="type" class="form-control show-tick" msg='Tipo' required data-live-search="true" >
            <option value="0">Seleccione Tipo</option>
            <option value="Comida">Comida</option>
            <option value="Bebida">Bebida</option>
        </select>
    </div>
</div>

<label for="sell_price">Precio Venta:</label>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">attach_money</i>
    </span>
    <div class="form-line">
        {!! Form::text('sell_price', null, ['class' => 'form-control col-green font-bold', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Precio Venta', 'placeholder' => "Ingrese Precio Venta " . $modulo_msg, 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>

<label for="checked_products">Productos en Comida o Bebida:</label>
<div class="input-group">
    <div id="div_check_products" msg='Al menos un Producto en Comida'>
        @if (count($data_products) > 0)
            @foreach ($data_products as $reg) 
                <div class="form-line"></div>
                <div class="row">
                    <input quantity_purchase="{{$reg->value_type}}" purchase_price_by_quantity="{{$reg->real_purchase_price}}" type_product_id="{{$reg->type}}" type="checkbox" name="check_product-{{$reg->id}}" onclick="setQuantityProduct(this.form.id, this.id, '{{$reg->id}}', '{{$reg->quantity_default_value}}');" id="check_product-{{$reg->id}}" class="filled-in chk-col-pink">
                    <label for="check_product-{{$reg->id}}"><b class="col-black">{{$reg->description}}</b> ({{$reg->value_type}} {{$reg->type_description}} por $ {{$reg->real_purchase_price}})</label>
                    <div class="pull-right">
                        <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <td>
                                    <button type="button" onclick="addRemoveQuantityProduct(this.form.id, '{{$reg->id}}', {{$reg->accumulator_value}});" class="btn bg-green btn-circle waves-effect waves-circle waves-float" >
                                        <i class="material-icons">add</i>
                                    </button> 
                                </td>
                                <td>
                                    <button type="button" onclick="addRemoveQuantityProduct(this.form.id, '{{$reg->id}}', -{{$reg->accumulator_value}});" class="btn bg-red btn-circle waves-effect waves-circle waves-float" >
                                        <i class="material-icons">remove</i>
                                    </button> 
                                </td>
                                <td>
                                    <label class="col-teal"><u>Cant ({{$reg->type_description}}):</u></label>
                                    {!! Form::text("quantity_product-" . $reg->id, 0, [
                                        'class' => 'bg-grey text-center quantityInputProduct', 
                                        'onkeypress' => 'return isNumberKey(event)',
                                        'onchange' => 'showCalcCostProductByQuantity(this.form.id, this.value, "check_product-' . $reg->id . '", "cost_price_product-' . $reg->id . '")',
                                        'onkeyup' => 'showCalcCostProductByQuantity(this.form.id, this.value, "check_product-' . $reg->id . '", "cost_price_product-' . $reg->id . '")',  
                                        'style' => 'width:40px;', 
                                        'data-rule' => 'quantity', 
                                        'autofocus' => true]) 
                                    !!}
                                </td>
                                <td>
                                    <label class="col-teal"><u>Costo:</u></label>
                                    <label class="col-red text-center label-money" id="cost_price_product-{{$reg->id}}" cost-product="0" name="cost_price_product-{{$reg->id}}">$ 0.00</label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            @endforeach
        @else
            <b class="col-red">No hay productos cargados en el sistema</b>
        @endif
    </div>
</div>

<label for="cost_price">Costo Total:</label>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">attach_money</i>
    </span>
    <div class="form-line">
        {!! Form::text('cost_price', null, ['class' => 'form-control col-red font-bold', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Precio Costo', 'placeholder' => "Precio Total Costo " . $modulo_msg, 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>