<label for="stock" class="col-red" id="label_stock">Stock Disponible: <b class="col-grey" id="label_unidad"></b></label>
<div class="input-group">
    <span class="input-group-addon">
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        {!! Form::text('stock', 0, ['class' => 'form-control col-red font-bold', 'disabled' => true, 'placeholder' => "Stock Disponible " . $modulo_msg, 'autofocus' => true]) !!}
    </div>
</div>
<label for="stock" class="col-green" id="label_stock">Incremento Stock: <b class="col-grey" id="label_unidad2"></b></label>
<div class="input-group">
    <span class="input-group-addon">
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        {!! Form::text('increment_stock', 0, ['class' => 'form-control col-green font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Incremento de Stock" . $modulo_msg, 'autofocus' => true]) !!}
    </div>
</div>
