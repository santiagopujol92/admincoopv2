<label for="description">Nombre:</label>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">description</i>
    </span>
    <div class="form-line">
        {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => "Ingrese Nombre " . $modulo_msg, 'required' => true, 'msg' => 'Nombre', 'autofocus' => true]) !!}
    </div>
</div>

<label for="id_sale_type_product">Tipo de {{$modulo_msg}}:</label>
<div class="input-group input-above2">
    <span class="input-group-addon">
        <label class="col-red">*</label>
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        <select name="id_sale_type_product" class="form-control show-tick" msg="Tipo de Producto Venta" required data-live-search="true" >
            <option value="0">Seleccione Tipo {{$modulo_msg}}</option>
            @foreach ($data_sale_type_products as $reg)
                <option value="{{$reg->id}}" >{{$reg->description}}</option>
            @endforeach
        </select>
    </div>
</div>

<label for="type_metric_product">Tipo de Medición de {{$modulo_msg}}:</label>
<div class="input-group input-above">
    <span class="input-group-addon">
        <label class="col-red">*</label>
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        <select name="type_metric_product" class="form-control show-tick" msg="Tipo de Medición" required data-live-search="true" >
            <option value="0">Seleccione Tipo de Medición de {{$modulo_msg}}</option>
            <option value="KG">KG</option>
            <option value="UNIDAD">UNIDAD</option>
        </select>
    </div>
</div>