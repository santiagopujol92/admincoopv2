<label for="type">Estado:</label>
<div class="input-group input-above2">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        <select name="id_status_order_by_table" class="form-control show-tick" msg="Estado" required data-live-search="true" >
            <option value="0">Seleccione un Estado</option>
            @foreach ($data_status_order_by_tables as $reg) 
                <option value="{{$reg->id}}">{{$reg->description}}</option>
            @endforeach
        </select>
    </div>
</div>


