<label for="id_person">Cliente:</label>
<div class="input-group input-above4">
    <span class="input-group-addon">
        <i class="material-icons">person</i>
    </span>
    <div class="form-line">
        <select name="id_person" class="form-control show-tick" msg="Cliente" data-live-search="true" >
            <option value="0">Seleccione un Cliente</option>
            @foreach ($data_clients as $reg) 
                <option value="{{$reg->id}}">{{$reg->lastname}} {{$reg->name}} @if (!empty($reg->cuit)) - {{$reg->cuit}} @endif</option>
            @endforeach
        </select>
    </div>
</div>

<label for="id_person_employee">Mozo:</label>
<div class="input-group input-above3">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">person</i>
    </span>
    <div class="form-line">
        <select name="id_person_employee" class="form-control show-tick" msg="Mozo" required data-live-search="true" >
            <option value="0">Seleccione un Mozo</option>
            @foreach ($data_employees as $reg) 
                <option value="{{$reg->id}}">{{$reg->name}} {{$reg->lastname}} @if (!empty($reg->cuit)) - {{$reg->cuit}} @endif</option>
            @endforeach
        </select>
    </div>
</div>

<label for="sell_date">Fecha/Hora Venta:</label>
<div class="input-group">
    <span class="input-group-addon">
        <i class="material-icons">date_range</i>
    </span>
    <div class="form-line">
        {!! Form::text('sell_date', null, ['class' => 'datetimepicker form-control', 'msg' => 'Fecha Venta', 'placeholder' => "Seleccione Fecha y Hora de Venta" ]) !!}
    </div>
</div>

<label for="div_check_foods">Comidas o Bebidas de la venta:</label>
<a id="btn_unlock_foods" class="btn btn-link bg-orange col-white waves-effect pull-right" disabled>Desbloquear y Modificar Comidas y Bebidas</a>
<div class="input-group">
    <div id="div_check_foods" style="overflow-y:scroll; height:400px;padding-left: 20px;" msg='Al menos una Comida'>
        @if (count($data_food) > 0)
            @foreach ($data_food as $reg) 
                <div class="form-line"></div>
                <div class="row" >
                    <input sell-price-food="{{$reg->sell_price}}" cost-price-food="{{$reg->cost_price}}" type="checkbox" name="check_food-{{$reg->id}}" onclick="setQuantityFood(this.form.id, this.id, '{{$reg->id}}');" id="check_food-{{$reg->id}}" class="filled-in chk-col-pink">
                    <label for="check_food-{{$reg->id}}">
                        <b class="col-black">{{$reg->description}}</b> <b @if ($reg->type == 'Bebida') class="col-teal" @else class="col-orange" @endif>({{$reg->type}})</b> <b>por $ {{$reg->sell_price}}</b></label>
                    <div class="pull-right">
                        <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <td>
                                    <button type="button" onclick="addRemoveQuantitySale(this.form.id, '{{$reg->id}}', 1);" class="btn bg-green btn-circle waves-effect waves-circle waves-float countButton" >
                                        <i class="material-icons">add</i>
                                    </button>
                                </td> 
                                <td>
                                    <button type="button" onclick="addRemoveQuantitySale(this.form.id, '{{$reg->id}}', -1);" class="btn bg-red btn-circle waves-effect waves-circle waves-float countButton" >
                                        <i class="material-icons">remove</i>
                                    </button>
                                </td>
                                <td> 
                                    <label class="col-teal"><u>Cantidad:</u></label>
                                    {!! Form::text("quantity_food-" . $reg->id, 0, [
                                        'class' => 'bg-grey text-center quantityInput', 
                                        'onkeypress' => 'return isNumberKey(event)',
                                        'onchange' => 'calcAndShowFoodByQuantity(this.form.id, this.value, "check_food-' . $reg->id . '", "cost_price_food-' . $reg->id . ', "sell_price_food-' . $reg->id . '")', 
                                        'onkeyup' => 'calcAndShowFoodByQuantity(this.form.id, this.value, "check_food-' . $reg->id . '", "cost_price_food-' . $reg->id . ', "sell_price_food-' . $reg->id . '")',  
                                        'style' => 'width:40px;', 
                                        'data-rule' => 'quantity', 
                                        'autofocus' => true]) 
                                    !!}
                                </td>
                                <td>
                                    <label class="col-teal">
                                        <u>Costo:</u>
                                    </label>
                                    <label class="col-red text-center label-money" id="cost_price_food-{{$reg->id}}" cost-price-food-with-quantity="0" name="cost_price_food-{{$reg->id}}">
                                    $ 0.00
                                    </label>
                                </td>
                                <td>
                                    <label class="col-teal">
                                        <u>Venta:</u>
                                    </label>
                                    <label class="col-green text-center label-money" id="sell_price_food-{{$reg->id}}" sell-price-food-with-quantity="0" name="sell_price_food-{{$reg->id}}">
                                    $ 0.00
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            @endforeach
        @else
            <b class="col-red">No hay comidas cargadas en el sistema</b>
        @endif
    </div>
</div>

<label for="cost_price">Costo Venta:</label>
<div class="input-group">
    <span class="input-group-addon">
        <i class="material-icons">attach_money</i>
    </span>
    <div class="form-line">
        {!! Form::text('cost_price', null, ['class' => 'form-control col-red font-bold', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Precio Costo', 'placeholder' => "Costo " . 'Venta', 'autofocus' => true]) !!}
    </div>
</div> 

<label for="sell_price">Precio Venta:</label>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">attach_money</i>
    </span>
    <div class="form-line">
        {!! Form::text('sell_price', null, ['class' => 'form-control col-green font-bold', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Precio Venta', 'placeholder' => "Ingrese Precio " . 'Venta', 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>