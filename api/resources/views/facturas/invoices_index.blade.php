@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection   
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>{{$modulo_msg}}s</h3>
            </div>
			<!-- LISTADO -->
	        <div class="card">
	        	<!-- MENSAGE --> 
                <div id="mensaje_principal"></div>
	            <!-- FIN MENSAJE -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="header">
	    		            <!-- LOADING2 -->
			                <div id="loading_list"></div>
			                <!-- FIN LOADING2 -->
	                     	<div class="icon-button-demo">
				                <button class="btn btn-link bg-pink waves-effect pull-right" data-backdrop="static" type="button" onclick="clearForm('form{{$form}}');configSaveButtonAdd();restartConfigForm();" data-toggle="modal" data-target="#modal{{$modals_btns}}">
	                                <div class="demo-google-material-icon"> 
	                                	{{-- <i class="material-icons">add_circle</i>  --}}
	                                	<span class="icon-name">NUEVA {{$titulo}}</span> 
	                               	</div>
				        		</button>
				        	</div>
			        		<h4>LISTADO DE {{$titulo}}S</h4>
	                    </div>
	                    <div class="body table-responsive">
	                        <div class="{{$modals_btns}}s">
		                        <table class="table table-bordered table-striped table-hover datatable dataTable">
		                            <thead>
		                                <tr>
		                                    <th>Nro. Factura</th>
		                                    <th>Cliente</th>
											<th>Material</th>
		                                    <th>Monto Total</th>
		                                    <th>Creación</th>
		                                    <th>Opciones</th>
		                                </tr>
		                            </thead>
		                            <tbody id="tbody_{{$module}}">
			                            @foreach ($data_controller as $reg) 
			                                <tr>
			                                    <td>{{$reg->invoice_number}}</td>
			                                    <td>
			                                    	{{-- SI EL ID_PERSON VIENE CON 1 O NULL ESPORQ NO TIENE PERSONA ENTONCES SE IMPRIME EMPRESA --}}
			                                    	@if ($reg->id_person == 1 || $reg->id_person == null)
														{{$reg->business}}
													@else
														{{$reg->lastname}} {{$reg->name}}
													@endif
			                                    </td>
			                                    <td>{{$reg->product}}</td>
			                                    <td>$ {{$reg->total_amount}}</td>
	                                    		<td>@if ($reg->created_at != '')
			                                    		{{Carbon\Carbon::parse($reg->created_at)->format('d/m/Y H:i:s')}}
													@else
			                                    		{{''}} 
			                                    	@endif
			                                    </td>
			                                    <td>
													<div class="icon-button-demo">
														<button type="button" href="#" data-backdrop="static" data-toggle="modal" title="Editar" onclick="mostrarEnModal({{$reg->id}});" data-target="#modal{{$modals_btns}}" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">
					                                    	<i class="material-icons">edit</i>
					                                   	</button>
				                    	             	<button type="button" href="#" data-backdrop="static" title="Eliminar" onclick="abrirModalEliminar({{$reg->id}});" data-toggle="modal" data-target="#modalDelete{{$modals_btns}}" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
				                            				<i class="material-icons">delete</i>
				                                		</button>
													</div>
			                                    </td>
			                                </tr>
			                            @endforeach 
		                            </tbody>
		                        </table>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--# LISTADO -->
        </div>
    </div>
    </section>

  	@include('global_modals.modal_edit_add_global')
	@include('global_modals.modal_delete_global')

@endsection

@section('scripts')
    <!-- Custom Js -->
	<script src="../scripts/{{$module}}.js"></script>
    <script src="../scripts/abm_basic.js"></script>
@endsection