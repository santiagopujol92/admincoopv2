<div class="col-sm-6">
    <div class="input-group">
        <div class="demo-radio-button">
            <input type="radio" name="type_person" class="required" id="radio_person" checked="true" onchange="showSelectPerson(this.form.id, this.value)" value="p" />
            <label for="radio__person">Persona</label>
            <input type="radio" name="type_person" class="required" id="radio_legal_person" onchange="showSelectPerson(this.form.id, this.value)" value="e"/> 
            <label for="radio_legal_person">Empresa</label>
        </div>
    </div>
</div>
<div class="col-sm-6" >
    <div class="input-group input-above4">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">person</i>
        </span>
        <div class="form-line">
            <select name="id_type_person" onchange="changeDataSelectTarget('personas', 'findByTypePersonId', 'id_person', 'Persona', this.form.id, this.value, 0, false);" class="form-control show-tick" required data-live-search="true" >
                <option value="0">Seleccione Tipo Persona o Empresa</option>
                @foreach ($data_type_persons as $reg) 
                    <option value="{{$reg->id}}">{{$reg->description}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-sm-12 hidden"  id="div_select_legal_person" >
    <div class="input-group input-above3">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">location_city</i>
        </span>
        <div class="form-line">
            <select name="id_legal_person" class="form-control show-tick" disabled required data-live-search="true" >
                {{-- <option value="0">Seleccione Empresa</option> --}}
                @foreach ($data_legal_people as $reg) 
                    <option value="{{$reg->id}}">{{$reg->name}} - {{$reg->cuit}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-sm-12" id="div_select_person">
    <div class="input-group input-above3">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">person</i>
        </span>
        <div class="form-line">
            <select name="id_person" class="form-control show-tick" disabled required data-live-search="true" >
                {{-- <option value="0">Seleccione Cliente</option> --}}
                @foreach ($data_people as $reg) 
                    <option value="{{$reg->id}}">{{$reg->name}} {{$reg->lastname}} - {{$reg->cuit}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">vpn_key</i>
        </span>
        <div class="form-line">
            {!! Form::text('invoice_number', null, ['class' => 'form-control', 'placeholder' => "Número Factura", 'required' => true, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">attach_money</i>
        </span>
        <div class="form-line">
            {!! Form::number('total_amount', null, ['class' => 'form-control', 'placeholder' => "Importe Total", 'required' => false, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="input-group input-above2">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">radio_button_checked</i>
        </span>
        <div class="form-line">
            <select name="id_status_invoice" class="form-control show-tick" required data-live-search="true" >
                <option value="0">Seleccione Estado Factura</option>
                @foreach ($data_status_invoices as $reg) 
                    <option value="{{$reg->id}}">{{$reg->description}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="input-group input-above2">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">attach_money</i>
        </span>
        <div class="form-line">
            <select name="id_payment_method" class="form-control show-tick" onchange="enableFieldsByPaymentMethod(this.form.id, this.value, 'div_tarjet_method_payment')" required data-live-search="true" >
                <option value="0">Seleccione Forma de Pago</option>
                @foreach ($data_payments_methods as $reg) 
                    <option value="{{$reg->id}}">{{$reg->description}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div id="div_tarjet_method_payment" class="hidden">
    <div class="col-sm-4">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">attach_money</i>
            </span>
            <div class="form-line">
                {!! Form::text('fees', null, ['class' => 'form-control', 'placeholder' => "Cuotas", 'autofocus' => true]) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">attach_money</i>
            </span>
            <div class="form-line">
                {!! Form::text('parcial_amount', null, ['class' => 'form-control', 'placeholder' => "Importe por Cuota", 'autofocus' => true]) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">attach_money</i>
            </span>
            <div class="form-line">
                {!! Form::text('administrative_expenses', null, ['class' => 'form-control', 'placeholder' => "Gastos Administrativos", 'autofocus' => true]) !!}
            </div>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="input-group input-above">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">shopping_basket</i>
        </span>
        <div class="form-line">
            <select name="id_type_product" onchange="changeDataSelectTarget('productos', 'findByTypeProductId', 'id_product', 'Producto', this.form.id, this.value);" class="form-control show-tick" required data-live-search="true" >
                <option value="0">Seleccione Tipo de Material</option>
                @foreach ($data_type_products as $reg) 
                    <option value="{{$reg->id}}">{{$reg->description}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="input-group input-above">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">shopping_basket</i>
        </span>
        <div class="form-line">
            <select name="id_product" class="form-control show-tick" disabled required data-live-search="true" >
                <option value="0">Seleccione Material</option>
                @foreach ($data_products as $reg) 
                    <option value="{{$reg->id}}">{{$reg->description}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">date_range</i>
        </span>
        <div class="form-line">
            {!! Form::text('issue_date', null, ['class' => 'datepicker form-control', 'placeholder' => "Seleccione Fecha de Emisión", 'required' => false, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="input-group">
        <span class="input-group-addon" >
            <label class="col-red">*</label> 
            <i class="material-icons">date_range</i>
        </span>
        <div class="form-line">
            {!! Form::text('effective_date', null, ['class' => 'datepicker form-control', 'placeholder' => "Seleccione Fecha de Vigencia", 'required' => false, 'autofocus' => true]) !!}
        </div>
    </div>
</div>
<div class="col-sm-2">
	<input type="checkbox" name="paid" onchange="enableInputAfterCheck(this.name, this.form.id, 'payment_date')" id="check_paid" class="filled-in chk-col-pink">
	<label for="check_paid">Pagada</label>
</div>
<div class="col-sm-10">
    <div class="input-group">
        <span class="input-group-addon" id="span_payment_date">
            <i class="material-icons">date_range</i>
        </span>
        <div class="form-line">
            {!! Form::text('payment_date', null, ['class' => 'datepicker form-control', 'placeholder' => "Seleccione Fecha de Pago", 'disabled' => true, 'autofocus' => true]) !!}
        </div>
    </div>
</div>