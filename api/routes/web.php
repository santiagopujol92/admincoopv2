<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
|api/ Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* RUTEO FRENTE */
Route::get('api/home', 'HomeController@index');

/* RUTEO LOG */
Route::resource('api/log', 'LogController');
Route::resource('api/', 'LogController');
Route::resource('api/login', 'LogController');

Route::get('api/logout', 'LogController@logout');

/* RUTEOS MODULOS ABM */
Route::resource('api/usuarios', 'UserController');
Route::resource('api/tipo_usuarios', 'TypeUserController');
Route::resource('api/tipo_productos', 'TypeProductController');
Route::resource('api/productos', 'ProductoController');
Route::resource('api/tipo_productos_venta', 'SaleTypeProductController');
Route::resource('api/productos_venta', 'SaleProductController');
Route::resource('api/condiciones_ivas', 'CondicionIvaController');
Route::resource('api/tipo_personas', 'TypePersonController');
Route::resource('api/formas_de_pago', 'PaymentMethodController');
Route::resource('api/estados_remitos', 'StatusShipmentController');
Route::resource('api/estados_facturas', 'StatusInvoiceController');
Route::resource('api/personas', 'PersonController');
Route::resource('api/registros_logs', 'AuditController');
Route::resource('api/paises', 'CountryController');
Route::resource('api/provincias', 'ProvinceController');
Route::resource('api/ciudades', 'CityController');
Route::resource('api/ventas', 'SaleController');
Route::resource('api/tipo_movimientos_cuentas', 'TypeMovementsAccountController');

Route::resource('api/comidas', 'FoodController');

/* RUTEOS DE PETICIONES DE DATOS PARA LLAMADAS AJAX, EN EL CONTROLADOR TRAER DATOS Y LLAMAR DESDE JS RUTA */
Route::get('api/usuarios_listar', 'UserController@listing');
Route::get('api/tipo_usuarios_listar', 'TypeUserController@listing');
Route::get('api/tipo_productos_listar', 'TypeProductController@listing');
Route::get('api/productos_listar', 'ProductoController@listing');
Route::get('api/tipo_productos_venta_listar', 'SaleTypeProductController@listing');
Route::get('api/productos_venta_listar', 'SaleProductController@listing');
Route::get('api/condiciones_ivas_listar', 'CondicionIvaController@listing');
Route::get('api/tipo_personas_listar', 'TypePersonController@listing');
Route::get('api/formas_de_pago_listar', 'PaymentMethodController@listing');
Route::get('api/estados_facturas_listar', 'StatusInvoiceController@listing');
Route::get('api/estados_remitos_listar', 'StatusShipmentController@listing');
Route::get('api/personas_listar', 'PersonController@listing');
Route::get('api/paises_listar', 'CountryController@listing');
Route::get('api/provincias_listar', 'ProvinceController@listing');
Route::get('api/ciudades_listar', 'CityController@listing');
Route::get('api/ventas_listar', 'SaleController@listing');
Route::get('api/tipo_movimientos_cuentas_listar', 'TypeMovementsAccountController@listing');

Route::get('api/comidas_listar', 'FoodController@listing');
Route::get('api/stocks_listar', 'StockController@listing');
Route::get('api/mesas_listar', 'TableController@listing');
Route::get('api/estados_mesas_listar', 'StatusTablesController@listing');
Route::get('api/bar_listar', 'BarController@listing');

/* RUTEOS PARA DEVOLVER DATA DE SELECTS A PARTIR DE UN VALOR DE OTRO SELECT. Funciones findBy..*/
Route::get('api/provincias_findByCountryId/{id}', 'ProvinceController@findByCountryId');
Route::get('api/ciudades_findByProvinceId/{id}', 'CityController@findByProvinceId');
Route::get('api/productos_findByTypeProductId/{id}', 'ProductoController@findByTypeProductId');
Route::get('api/personas_findByTypePersonId/{id}', 'PersonController@findByTypePersonId');
Route::get('api/ventas_getAllSales', 'SaleController@getAllSales');
Route::get('api/productos_findBySaleTypeProductId/{id}', 'SaleProductController@findBySaleTypeProductId');

/* RUTEOS PARA OTRA DATA DE FUNCIONES ESPECIALES*/
Route::get('api/home_getCountsDataForHome', 'HomeController@getCountsDataForHome');
Route::get('api/registros_logs_getDataAuditToNotificaction/{limit}', 'AuditController@getDataAuditToNotificaction');
Route::get('api/ventas_destroyFoodsBySale/{id}', 'SaleController@destroyFoodsBySale');
Route::get('api/mesas_createOrderByTable', 'TableController@createOrderByTable');

/*OTROS RUTEOS*/

Auth::routes();